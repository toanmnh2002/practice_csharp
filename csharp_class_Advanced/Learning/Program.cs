﻿using System.ComponentModel;
using System.Runtime.InteropServices;

public class Program
{
    Dictionary<int, int> dictionary = new Dictionary<int, int>();
    public void Add()
    {
        bool check;
        int key = 0;
        Console.Write("Enter key: ");
        int name;
        do
        {
            Console.Write("Enter name: ");
            check = int.TryParse(Console.ReadLine(), out name);
        } while (!check);
        if (!dictionary.ContainsKey(key) && !dictionary.ContainsValue(name))
        {
            dictionary.Add(key, name);
        }
        else
        {
            dictionary[key] = name;
        }
    }
    public void Print()
    {
        foreach (var item in dictionary)
        {
            Console.WriteLine($"key:{item.Key},Value:{item.Value}");
        }
    }
}

//Dictionary<int, int> dict = new Dictionary<int, int> { { 5, 4 }, { 6, 2 }, { 4, 3000 }, }; Console.WriteLine(dict[4]);
