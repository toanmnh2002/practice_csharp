﻿using System.Globalization;

class Program
{
    static void Main(string[] args)
    {
        int choice = 0;
        var a = new ShoppingList();
        bool check;
        do
        {
            Console.WriteLine($"|-----------------------------    Menu   -------------------------------------|");
            Console.WriteLine($"-------------------------------------------------------------------------------");
            Console.WriteLine($"|        1. Add item                                                          |");
            Console.WriteLine($"|        2. Remove item                                                       |");
            Console.WriteLine($"|        3. Display List                                                      |");
            Console.WriteLine($"|        4. Exit                                                              |");
            Console.WriteLine($"-------------------------------------------------------------------------------");
            do
            {
                Console.Write("Choose your option: ");
                check = int.TryParse(Console.ReadLine(), out choice);
            } while (!check);

            switch (choice)
            {
                case 1:
                    Console.Clear();
                    a.AddList();
                    break;
                case 2:
                    Console.Clear();
                    a.RemoveList();
                    break;
                case 3:
                    Console.Clear();
                    a.Print();
                    break;
                case 4: break;
            }
        } while (choice != 0);
    }
}