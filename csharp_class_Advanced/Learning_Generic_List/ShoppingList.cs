﻿public class ShoppingList
{
    List<string> list = new List<string>();
    public void AddList()
    {
        string item;
        bool check;
        do
        {
            Console.WriteLine("Enter Item: ");
            item = Console.ReadLine();
            check = !string.IsNullOrEmpty(item) && item.All(c => Char.IsLetter(c) || Char.IsWhiteSpace(c));
        } while (!check);
        if (!list.Contains(item))
        {
            list.Add(item);
            Console.WriteLine("Added!");
        }
        else
        {
            Console.WriteLine("Duplicate item!");
        }
    }
    public void RemoveList()
    {
        bool check;
        string item;
        do
        {
            Console.WriteLine("Enter Item to remove: ");
            item = Console.ReadLine();
            check = !string.IsNullOrEmpty(item) && item.All(c => Char.IsLetter(c) || Char.IsWhiteSpace(c));
        } while (!check);
        if (list.Count > 0)
        {
            list.Remove(item);
            Console.WriteLine("Removed!");
        }
        else
        {
            Console.WriteLine("Not contain item!");
        }
    }
    public void Print()
    {
        foreach (var item in list)
        {
            Console.WriteLine(item + " ");
        }
    }

}
