﻿//Pair<int, string, int, string> hello = new Pair<int, string, int, string>(10, "Toan", 4, "Nguyen");
//hello.loop(hello);
//public class Pair<T, O, A, N>
//{
//    public T First { get; set; }
//    public O Second { get; set; }
//    public A Third { get; set; }
//    public N Fourth { get; set; }

//    public Pair(T first, O second, A third, N fourth)
//    {
//        First = first;
//        Second = second;
//        Third = third;
//        Fourth = fourth;
//    }
//    public void loop<T>(T obj)
//    {
//        var a = typeof(T).GetProperties();
//        foreach (var item in a)
//        {
//            var value = item.GetValue(obj);
//            Console.WriteLine($"{item.Name}:{value}");
//        }
//    }
//}

//int a = 5;
//int b = 10;
//swap(ref a, ref b);
//void swap<T>(ref T a, ref T b)
//{
//    T temp = a;
//    a = b;
//    b = temp;
//}


//var a = new int[]
//{
//    1,2, 3, 4
//};
//var b = new string[]
//{
//    "felix","enzo"
//};

//constraint
//T GetLastItem<T>(T[] a) where T : class
//{
//    return a[a.Length - 1];
//}
//Console.WriteLine(GetLastItem(a));
//Console.WriteLine(GetLastItem(b));

//list and var
//var a = new List<string> { "1", "22", "334" };
//foreach (var item in a)
//{
//    Console.Write(item + " ");
//}
//List<int> a = new List<int> { 1, 2, 3 };
//List<int> b = new List<int> { 1,2};
//a.AddRange(a);
//Console.WriteLine(a[4]);
//foreach (int i in a)
//{
//    Console.WriteLine(i + " ");
//}
//if (a.Equals(b))
//{
//    Console.WriteLine("true");
//}
//else
//{
//    Console.WriteLine("false");
//}


