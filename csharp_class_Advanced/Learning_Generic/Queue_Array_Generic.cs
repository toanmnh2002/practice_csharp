﻿
public class Queue_Array_Generic<T>
{

    const int maxQueue = 4;
    T[] a = new T[maxQueue];
    int head = -1;
    int count = 0;
    public void Enqueue(T number)
    {
        //CHECK QUEUE IS FULL
        if (count == maxQueue)
        {
            throw new InvalidOperationException();
        }
        else if (count == 0)//CHECK QUEUE IS EMPTY
        {
            a[0] = number;//number is in index 0
            head = 0;//head pont to index 0
            count = 1;//increase count
        }
        else if (head + count < maxQueue)//CHECK SPACE FOR NUMBER
        {
            a[head + count] = number;//add new number for index next 
        }
        else
        {
            a[maxQueue - (head + count)] = number;//CHECK IS FULL RETURN TO THE INDEX 0
        }
        count++; //increase the item of array
    }

    public T Dequeue()
    {
        if (count == 0)
        {
            throw new InvalidOperationException();
        }

        T number = a[head];

        if (head == maxQueue - 1)
        {
            if (count == 1)
            {
                head = -1;
            }
            else
            {
                head = 0;
            }
        }
        else
        {
            head++;
        }
        count--;
        return number;
    }

    public T Peek()
    {
        if (head == -1 || count == 0)
        {
            throw new InvalidOperationException();
        }

        return a[head];
    }

}

