﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace Learning_Generic
//{
//    public class Queue<T>
//    {
//        private List<T> queue = new List<T>();
//        public void Enqueue(T item)
//        {
//            queue.Add(item);
//        }

//        public T Dequeue()
//        {
//            if (queue.Count == 0)
//                throw new InvalidOperationException("Queue is empty");

//            T ret = queue[0];
//            queue.RemoveAt(0);

//            return ret;
//        }

//        public T Peek()
//        {
//            if (queue.Count == 0)
//                throw new InvalidOperationException("Queue is empty");

//            return queue[0];
//        }

//        public int Count
//        {
//            get { return queue.Count; }
//        }

//        public void Clear()
//        {
//            queue.Clear();
//        }
//    }
//}
