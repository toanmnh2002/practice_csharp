﻿class Program
{
    static void Main()
    {
        //Queue<int> myQueue = new Queue<int>();
        //myQueue.Enqueue(1);
        //myQueue.Enqueue(2);
        //myQueue.Enqueue(3);

        //Console.WriteLine(myQueue.Dequeue());
        //Console.WriteLine(myQueue.Peek());

        var queue = new Queue_Array_Generic<string>(); // Example usage with strings
        queue.Enqueue("apple");
        queue.Enqueue("banana");

        Console.WriteLine(queue.Peek()); // apple

        string firstOut = queue.Dequeue();
        Console.WriteLine(firstOut); // apple

        queue.Enqueue("cherry");
    }
}