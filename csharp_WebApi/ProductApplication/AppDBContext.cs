﻿using Microsoft.EntityFrameworkCore;
using Ecommerce.Enities;

public class AppDBContext : DbContext
{
    public AppDBContext(DbContextOptions<AppDBContext> options) : base(options) { }
    public DbSet<Product> Products { get; set; }
}
