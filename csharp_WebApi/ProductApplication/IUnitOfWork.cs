﻿using Ecommerce.IRepository;

namespace Ecommerce
{
    public interface IUnitOfWork
    {
        public IProductRepository ProductRepository { get; }
        public Task<int> SaveChangeAsync();
    }
}
