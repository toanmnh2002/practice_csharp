﻿using Ecommerce.Entities;

namespace Ecommerce.Enities
{
    public class Product:BaseEntity
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
    }
}
