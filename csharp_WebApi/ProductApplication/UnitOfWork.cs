﻿using Ecommerce.IRepository;
using Ecommerce.Repository;

namespace Ecommerce
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDBContext _dbContext;
        private readonly IProductRepository _productRepository;

        public UnitOfWork(AppDBContext dbContext)
        {
            _dbContext = dbContext;
            _productRepository = new ProductRepository(_dbContext);
        }

        public IProductRepository ProductRepository => _productRepository;
        public async Task<int> SaveChangeAsync() => await _dbContext.SaveChangesAsync();
    }
}
