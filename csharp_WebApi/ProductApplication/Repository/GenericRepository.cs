﻿using Microsoft.EntityFrameworkCore;
using Ecommerce.Entities;
using Ecommerce.IRepository;
using System.Linq.Expressions;

namespace Ecommerce.Repository
{
    public class GenericRepository<T> : IGenericRepository<T> where T : BaseEntity
    {
        protected DbSet<T> _dbSet;
        private readonly AppDBContext _appDBContext;

        public GenericRepository(AppDBContext appDBContext)
        {
            _dbSet = appDBContext.Set<T>();
            _appDBContext = appDBContext;
        }

        public async Task AddEntityAsync(T obj)
        {
            await _dbSet.AddAsync(obj);
        }
        public async Task AddEntityRange(ICollection<T> objs)
        {
            await _dbSet.AddRangeAsync(objs);
        }
        public void UpdateEntity(T obj)
        {
            _dbSet.Update(obj);
        }
        public void UpdateRange(ICollection<T> objs)
        {
            _dbSet.UpdateRange(objs);
        }
        public void SoftRemoveEntity(T obj)
        {
            _dbSet.Remove(obj);
        }

        public void SoftRemoveEntityRange(ICollection<T> objs)
        {
            _dbSet.RemoveRange(objs);
        }
        public async Task<IEnumerable<T>> GetListAsync()
        {
            var entities = await _dbSet.AsQueryable().ToListAsync();
            return entities;
        }
        public async Task<T?> GetEntityByIdAsync(int id)
        {
            var result = await _dbSet.FindAsync(id);
            //var result = await _dbSet.FirstOrDefaultAsync(x => x.Id == id);
            return result;
        }
    }
}
