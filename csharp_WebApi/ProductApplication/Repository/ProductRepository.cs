﻿using Ecommerce.Enities;
using Ecommerce.IRepository;

namespace Ecommerce.Repository
{
    public class ProductRepository : GenericRepository<Product>, IProductRepository
    {
        public ProductRepository(AppDBContext appDBContext) : base(appDBContext)
        {
        }
    }
}
