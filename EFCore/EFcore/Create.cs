﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFcore
{
    public class Create
    {
        public async static void Add()
        {
            var context = new AppDbContext();
            var a = new Person { Name = "A", };
            Console.WriteLine(context.Entry(a));
            context.Persons.Add(a);
            Console.WriteLine(context.Entry(a));
            context.SaveChanges();
            Console.WriteLine(context.Entry(a));
        }
        public static void View()
        {
            var context = new AppDbContext();
            var persons = context.Persons.Select(x => x.Name).ToList();
            foreach (var person in persons)
            {
                Console.WriteLine($"{person}");
            }
        }
        public static void Update()
        {
            var context = new AppDbContext();
            var a = context.Persons.FirstOrDefault(x => x.Id == 5);
            Console.WriteLine(context.Entry(a));
            a.Name = "toaneeeeeeeeeee";
            context.SaveChanges();
            Console.WriteLine(context.Entry(a));
        }
        public static void Delete()
        {
            var context = new AppDbContext();
            var a = context.Persons.FirstOrDefault(x => x.Id == 5);
            Console.WriteLine(context.Entry(a));
            context.Remove(a);
            context.SaveChanges();
            Console.WriteLine(context.Entry(a));
        }
        public static void Tracking()
        {
            var context = new AppDbContext();
            //var a = context.Persons.AsNoTracking().FirstOrDefault(x => x.Id == 7);
            //a.Name = "ToannGUYEN";
            //context.Entry(a).State = EntityState.Deleted;
            ////context.Update(a);
            //context.SaveChanges();
            if (context.Persons.Any())
            {
                return;
            }

        }
    }
}
