﻿using Microsoft.EntityFrameworkCore;




public class AppDbContext : DbContext
{
    public DbSet<Person> Persons { get; set; }
    public DbSet<Address> Address { get; set; }
    public DbSet<DateOfBirth> DateOfBirths { get; set; }
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseSqlServer("Data Source=NGUYENMANHTOAN;Initial Catalog=toan;Integrated Security=True;Persist Security Info=False;User ID=sa;Password=123;Pooling=False;MultipleActiveResultSets=False;Encrypt=False;TrustServerCertificate=True");
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Person>().HasData(
            new Person { Id = 1, Name = "haha" }
            );
    }
    
}
