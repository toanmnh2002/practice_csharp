﻿using MovieApplication.IRepository;
using MovieApplication.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieApplication
{
    public interface IUnitOfWork:IDisposable
    {
        public IMovieRepository MovieRepository { get; }
        public IReviewRepository ReviewRepository { get; }
        public Task<int> SaveChangeAsync();
    }
}
