﻿using MovieApplication.Entities;
using MovieApplication.IRepository;
using MovieApplication.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieApplication.Repositories
{
    public class ReviewRepository : GenericRepository<Review>, IReviewRepository
    {
        public ReviewRepository(AppDBcontext appDBContext) : base(appDBContext)
        {
        }
    }
}
