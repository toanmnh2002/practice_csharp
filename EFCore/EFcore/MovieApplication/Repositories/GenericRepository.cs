﻿using MovieApplication;
using MovieApplication.Entities;
using MovieApplication.IRepository;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;


namespace MovieApplication.Repository
{
    public class GenericRepository<T> : IGenericRepository<T> where T : BaseEntity
    {
        protected DbSet<T> _dbSet;
        private readonly AppDBcontext _appDBContext;

        public GenericRepository(AppDBcontext appDBContext)
        {
            _dbSet = appDBContext.Set<T>();
            _appDBContext = appDBContext;
        }

        public async Task AddEntityAsync(T obj)
        {
            await _dbSet.AddAsync(obj);
        }
        public async Task AddEntityRange(ICollection<T> objs)
        {
            await _dbSet.AddRangeAsync(objs);
        }
        public void UpdateEntity(T obj)
        {
            _dbSet.Update(obj);
        }
        public void UpdateRange(ICollection<T> objs)
        {
            _dbSet.UpdateRange(objs);
        }
        public void SoftRemoveEntity(T obj)
        {
            _dbSet.Remove(obj);
        }

        public void SoftRemoveEntityRange(ICollection<T> objs)
        {
            _dbSet.RemoveRange(objs);
        }
        public async Task<ICollection<T>> GetListAsync()
        {
            var entities = await _dbSet.ToListAsync();
            return entities;
        }
        public async Task<T?> GetEntityByIdAsync(int id)
        {
            var result = await _dbSet.FirstOrDefaultAsync(x => x.Id == id);
            return result;
        }
    }
}
