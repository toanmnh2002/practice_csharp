﻿using Microsoft.EntityFrameworkCore;
using MovieApplication.Entities;
using MovieApplication.IRepository;
using MovieApplication.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieApplication.Repositories
{
    public class MovieRepository : GenericRepository<Movie>, IMovieRepository
    {
        public MovieRepository(AppDBcontext appDBContext) : base(appDBContext)
        {
        }
        public async Task<Movie> GetMovieReview(int id)=> await _dbSet.Include(x => x.Reviews).FirstOrDefaultAsync(x => x.Id == id);
    }
}
