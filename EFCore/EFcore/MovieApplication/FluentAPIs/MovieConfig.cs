﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.ValueGeneration;
using MovieApplication.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieApplication.FluentAPIs
{
    public class MovieIdValueGenerator : ValueGenerator<int>
    {
        private int _currentValue = -1;

        public override bool GeneratesTemporaryValues => false;

        public override int Next(EntityEntry entry)
        {
            return _currentValue--;
        }
    }
    public class MovieConfig : IEntityTypeConfiguration<Movie>
    {
        public void Configure(EntityTypeBuilder<Movie> builder)
        {
            builder.Property(e => e.Id).HasValueGenerator<MovieIdValueGenerator>();
            builder.HasIndex(a => a.Title).IsUnique();
            builder.HasData(
                new Movie
                {
                    Title = "Squirt Game",
                    ReleaseDate = new DateTime(2023, 02, 2),
                    Director = "ToanNguyen",
                    Description = "Good",
                    ImageUrl = "lovely",
                    Rating = 5
                },
                new Movie
                {
                    Title = "Herculess",
                    ReleaseDate = new DateTime(2023, 5, 5),
                    Director = "chs",
                    Description = "hay",
                    ImageUrl = "vip",
                    Rating = 5
                }
                );
        }
    }
}
