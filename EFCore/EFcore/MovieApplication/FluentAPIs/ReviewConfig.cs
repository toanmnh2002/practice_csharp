﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MovieApplication.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieApplication.FluentAPIs
{
    public class ReviewConfig : IEntityTypeConfiguration<Review>
    {
        public void Configure(EntityTypeBuilder<Review> builder)
        {
            builder.HasOne(a => a.Movie)
                .WithMany(a => a.Reviews)
                .HasForeignKey(a => a.MovieId);
            builder.HasData(
                new Review
                {
                    Id = -1,
                    MovieId = 1,
                    Rating = 5,
                    Content = "This movie was amazing!",
                    Title = "Great movie"
                },
                new Review
                {
                    Id = -2,
                    MovieId = 2,
                    Rating = 4,
                    Content = "I enjoyed this movie.",
                    Title = "Good movie"
                });
        }
    }
}
