﻿using MovieApplication.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieApplication.IRepository
{
    public interface IReviewRepository:IGenericRepository<Review>
    {
    }
}
