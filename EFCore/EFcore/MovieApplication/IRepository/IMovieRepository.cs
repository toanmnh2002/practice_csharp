﻿using MovieApplication.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieApplication.IRepository
{
    public interface IMovieRepository : IGenericRepository<Movie>
    {
        Task<Movie> GetMovieReview(int id);
    }
}
