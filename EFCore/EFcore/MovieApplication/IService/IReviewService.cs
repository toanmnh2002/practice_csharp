﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieApplication.IService
{
    public interface IReviewService
    {
        Task ViewAllReview();
        Task AddReview();
        Task UpdateReview();
        Task DeleteReview();
    }
}
