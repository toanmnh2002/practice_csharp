﻿using MovieApplication.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieApplication.IService
{
    public interface IMovieService
    {
        Task ViewAllMovies();
        Task AddMovie();
        Task UpdateMovie();
        Task DeleteMovie();
        Task GetAllReviewers();
    }
}
