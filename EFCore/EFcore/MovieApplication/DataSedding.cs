﻿//using MovieApplication.Entities;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Runtime.CompilerServices;
//using System.Text;
//using System.Threading.Tasks;

//namespace MovieApplication
//{
//    public class DataSedding
//    {
//        private readonly AppDBcontext appDBcontext;

//        public DataSedding(AppDBcontext appDBcontext)
//        {
//            this.appDBcontext = appDBcontext;
//        }
//        public void SeedData()
//        {
//            if (!appDBcontext.Movies.Any())
//            {
//                var movies = new List<Movie>
//                {
//                    new Movie
//                    {
//                        Title = "The Shawshank Redemption",
//                        ReleaseDate = new DateTime(1994, 9, 23),
//                        Director = "Frank Darabont",
//                        Description = "Two imprisoned men bond over a number of years, finding solace and eventual redemption through acts of common decency.",
//                        ImageUrl = "https://www.imdb.com/title/tt0111161/mediaviewer/rm280607872",
//                        Rating = 9.3m,
//                        Reviews = new List<Review>
//                        {
//                            new Review
//                            {
//                                Title = "One of the best movies ever made",
//                                Content = "This movie is a masterpiece. The acting, the story, the music, everything is perfect.",
//                                Rating = 10
//                            },
//                            new Review
//                            {
//                                Title = "A powerful and moving film",
//                                Content = "The Shawshank Redemption is one of those rare films that can make you laugh and cry, and leave you feeling inspired.",
//                                Rating = 9
//                            }
//                        }
//                    },
//                    new Movie
//                    {
//                        Title = "The Godfather",
//                        ReleaseDate = new DateTime(1972, 3, 24),
//                        Director = "Francis Ford Coppola",
//                        Description = "The aging patriarch of an organized crime dynasty transfers control of his clandestine empire to his reluctant son.",
//                        ImageUrl = "https://www.imdb.com/title/tt0068646/mediaviewer/rm2939289344",
//                        Rating = 9.2m,
//                        Reviews = new List<Review>
//                        {
//                            new Review
//                            {
//                                Title = "A classic masterpiece",
//                                Content = "The Godfather is a timeless classic that has stood the test of time. It is a brilliantly acted, directed and written film that deserves all the praise it gets.",
//                                Rating = 10
//                            },
//                            new Review
//                            {
//                                Title = "A gripping drama",
//                                Content = "The Godfather is a gripping and intense drama that will keep you on the edge of your seat from start to finish.",
//                                Rating = 8
//                            }
//                        }
//                    }
//                };
//                appDBcontext.Movies.AddRange(movies);
//                appDBcontext.SaveChanges();
//            }
//        }
//    }
//}

