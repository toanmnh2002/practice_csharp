﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieApplication.Entities
{
    public class Movie:BaseEntity
    {
        public string Title { get; set; }
        public DateTime ReleaseDate { get; set; }
        public string Director { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public decimal Rating { get; set; }
        public ICollection<Review>? Reviews { get; set; }

        public Movie(string title, DateTime releaseDate, string director, string description, string imageUrl, decimal rating, ICollection<Review>? reviews)
        {
            Title = title;
            ReleaseDate = releaseDate;
            Director = director;
            Description = description;
            ImageUrl = imageUrl;
            Rating = rating;
            Reviews = reviews;
        }

        public Movie(string title, DateTime releaseDate, string director, string description, string imageUrl, decimal rating)
        {
            Title = title;
            ReleaseDate = releaseDate;
            Director = director;
            Description = description;
            ImageUrl = imageUrl;
            Rating = rating;
        }

        public Movie()
        {
        }
    }

}
