﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieApplication.Entities
{
    public class Review:BaseEntity
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public int Rating { get; set; }
        public int MovieId { get; set; }
        public Movie Movie { get; set; }

        public Review(string title, string content, int rating, int movieId, Movie movie)
        {
            Title = title;
            Content = content;
            Rating = rating;
            MovieId = movieId;
            Movie = movie;
        }

        public Review(string title, string content, int rating, int movieId)
        {
            Title = title;
            Content = content;
            Rating = rating;
            MovieId = movieId;
        }

        public Review()
        {
        }
    }
}
