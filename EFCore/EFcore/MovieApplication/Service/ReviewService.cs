﻿using Assignment.Util;
using MovieApplication.Entities;
using MovieApplication.IService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieApplication.Service
{
    public class ReviewService : IReviewService
    {
        private readonly IUnitOfWork _unitOfWork;

        public ReviewService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task AddReview()
        {
            var title = Inputter.validateString("Enter Title: ");
            //Console.Write("Enter Release Date: ");
            //var date = Convert.ToDateTime(Console.ReadLine());
            var content = Inputter.validateString("Enter Content: ");
            var rating = Inputter.validateNumInt("Enter Rating: ");
            var movieId = Inputter.validateNumInt("Enter movieId: ");
            var review = new Review(title, content, rating, movieId);
            await _unitOfWork.ReviewRepository.AddEntityAsync(review);
            var isSuccess = await _unitOfWork.SaveChangeAsync() > 0 ? "Sucess" : "fail";
            Console.WriteLine(isSuccess);
        }

        public async Task DeleteReview()
        {
            var id = Inputter.validateNumInt("Enter id: ");
            var obj = await _unitOfWork.ReviewRepository.GetEntityByIdAsync(id);
            if (obj is not null)
            {
                _unitOfWork.ReviewRepository.SoftRemoveEntity(obj);
                await _unitOfWork.SaveChangeAsync();
                Inputter.greenColor("Delete successfully");
            }
            else
            {
                Inputter.redColor($"Not found movieId:{id}");
            }
        }

        public async Task UpdateReview()
        {
            var id = Inputter.validateNumInt("Enter id: ");
            var obj = await _unitOfWork.ReviewRepository.GetEntityByIdAsync(id);
            if (obj is not null && obj.Id == id)
            {
                var title = Inputter.validateString("Enter Title: ");
                //Console.Write("Enter Release Date: ");
                //var date = Convert.ToDateTime(Console.ReadLine());
                var content = Inputter.validateString("Enter Content: ");
                var rating = Inputter.validateNumInt("Enter Rating: ");
                obj.Title = string.IsNullOrEmpty(title) ? obj.Title : title;
                obj.Content = string.IsNullOrEmpty(content) ? obj.Content : content;
                obj.Rating = rating;
                _unitOfWork.ReviewRepository.UpdateEntity(obj);
                await _unitOfWork.SaveChangeAsync();
                Inputter.greenColor("Update successfully");
            }
            else
            {
                Inputter.redColor($"Not found movieId:{id}");
            }
        }

        public async Task ViewAllReview()
        {
            var listReviews = await _unitOfWork.ReviewRepository.GetListAsync();
            if (listReviews is not null)
            {
                foreach (var item in listReviews)
                {
                    Console.WriteLine($"Id:{item.Id}\t" +
                        $"Title:{item.Title}\t" +
                        $"Rating:{item.Rating}\t" +
                        $"Content:{item.Content}\t" +
                        $"MovieId:{item.MovieId}\t");
                }
            }
            else
            {
                Inputter.redColor("List review is empty!");
            }
        }
        public static async Task ReviewMenu()
        {
            bool cont = false;
            using (var unit = new UnitOfWork(new AppDBcontext()))
            {
                do
                {
                    Console.WriteLine($"\n|-----------------------------    Review Menu   ----------------------------------------|");
                    Console.WriteLine($"------------------------------------------------------------------------------------");
                    Console.WriteLine($"|        1. Add Reviews                                                             |");
                    Console.WriteLine($"|        2. Update Reviews                                                          |");
                    Console.WriteLine($"|        3. Delete Reviews                                                          |");
                    Console.WriteLine($"|        4. Get all Reviews                                                         |");
                    Console.WriteLine($"|        5. Exit                                                                    |");
                    Console.WriteLine($"------------------------------------------------------------------------------------");
                    int choice = Inputter.validateNumInt("Enter your choice: ", 5);
                    var service = new ReviewService(unit);
                    switch (choice)
                    {
                        case 1:
                            await service.AddReview();
                            break;
                        case 2:
                            await service.UpdateReview();
                            break;
                        case 3:
                            await service.DeleteReview();
                            break;
                        case 4:
                            await service.ViewAllReview();
                            break;
                        case 5:
                            return;
                    }
                } while (!cont);
            }
        }
    }
}
