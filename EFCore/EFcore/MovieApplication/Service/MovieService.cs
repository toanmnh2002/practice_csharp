﻿
using Assignment.Util;
using MovieApplication;
using MovieApplication.Entities;
using MovieApplication.IService;

namespace MovieApplication.Service
{
    public class MovieService : IMovieService
    {
        private readonly IUnitOfWork _unitOfWork;

        public MovieService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task AddMovie()
        {
            var title = Inputter.validateString("Enter Title: ");
            //Console.Write("Enter Release Date: ");
            //var date = Convert.ToDateTime(Console.ReadLine());
            var director = Inputter.validateString("Enter Director: ");
            var description = Inputter.validateString("Enter Description: ");
            var imageUrl = Inputter.validateString("Enter ImageUrl: ");
            var rating = Inputter.validateNumDecimal("Enter Rating: ");
            var movie = new Movie(title, DateTime.Now, director, description, imageUrl, rating);
            await _unitOfWork.MovieRepository.AddEntityAsync(movie);
            var isSuccess = await _unitOfWork.SaveChangeAsync() > 0 ? "Sucess" : "fail";
            Console.WriteLine(isSuccess);
        }

        public async Task DeleteMovie()
        {
            var id = Inputter.validateNumInt("Enter id: ");
            var obj = await _unitOfWork.MovieRepository.GetEntityByIdAsync(id);
            if (obj is not null)
            {
                _unitOfWork.MovieRepository.SoftRemoveEntity(obj);
                await _unitOfWork.SaveChangeAsync();
                Inputter.greenColor("Delete successfully");
            }
            else
            {
                Inputter.redColor($"Not found movieId:{id}");
            }
        }

        public async Task GetAllReviewers()
        {
            var id = Inputter.validateNumInt("Enter id: ");
            var obj = await _unitOfWork.MovieRepository.GetMovieReview(id);
            if (obj is not null && obj.Id == id)
            {
                Console.WriteLine($"Id:{obj.Id}\t" +
                                        $"Title:{obj.Title}\t" +
                                        $"Datetime:{obj.ReleaseDate}\t" +
                                        $"Director:{obj.Director}\t" +
                                        $"Description:{obj.Description}\t" +
                                        $"ImageUrl:{obj.ImageUrl}\t" +
                                        $"Rating:{obj.Rating}");
                if (obj.Reviews.Any())
                {
                    foreach (var review in obj.Reviews.ToList())
                    {
                        Console.WriteLine($"Title: {review.Title}; " +
                                                    $"Content: {review.Content}; " +
                                                    $"Rating: {review.Rating}");
                    }
                }
                else
                {
                    Inputter.redColor("No reviews found.");
                }
            }
            else
            {
                Inputter.redColor($"Not found movieId:{id}");
            }
        }

        public async Task UpdateMovie()
        {
            var id = Inputter.validateNumInt("Enter id: ");
            var obj = await _unitOfWork.MovieRepository.GetEntityByIdAsync(id);
            if (obj is not null && obj.Id == id)
            {
                var Title = Inputter.validateString("Enter Title: ");
                //Console.Write("Enter Release Date: ");
                //var date = Convert.ToDateTime(Console.ReadLine());
                var Director = Inputter.validateString("Enter Director: ");
                var Description = Inputter.validateString("Enter Description: ");
                var ImgUrl = Inputter.validateString("Enter ImageUrl: ");
                var Rating = Inputter.validateNumDecimal("Enter Rating: ");
                obj.Title = string.IsNullOrEmpty(Title) ? obj.Title : Title;
                obj.Director = string.IsNullOrEmpty(Director) ? obj.Director : Director;
                obj.Description = string.IsNullOrEmpty(Description) ? obj.Description : Description;
                obj.ImageUrl = string.IsNullOrEmpty(ImgUrl) ? obj.ImageUrl : ImgUrl;
                obj.Rating = Rating;
                _unitOfWork.MovieRepository.UpdateEntity(obj);
                await _unitOfWork.SaveChangeAsync();
                Inputter.greenColor("Update successfully");
            }
            else
            {
                Inputter.redColor($"Not found movieId:{id}");
            }
        }

        public async Task ViewAllMovies()
        {
            var listmovies = await _unitOfWork.MovieRepository.GetListAsync();
            if (listmovies is not null)
            {
                foreach (var item in listmovies)
                {
                    Console.WriteLine($"Id:{item.Id}\t" +
                        $"Title:{item.Title}\t" +
                        $"Datetime:{item.ReleaseDate}\t" +
                        $"Director:{item.Director}\t" +
                        $"Description:{item.Description}\t" +
                        $"ImageUrl:{item.ImageUrl}\t" +
                        $"Rating:{item.Rating}");
                }
            }
            else
            {
                Inputter.redColor("List movie is empty!");
            }
        }
        public static async Task MovieMenu()
        {
            bool cont = false;
            using (var unit = new UnitOfWork(new AppDBcontext()))
            {
                do
                {
                    Console.WriteLine($"\n|-----------------------------    Movie Menu   ----------------------------------------|");
                    Console.WriteLine($"------------------------------------------------------------------------------------");
                    Console.WriteLine($"|        1. Add Movies                                                             |");
                    Console.WriteLine($"|        2. Update Movies                                                          |");
                    Console.WriteLine($"|        3. Delete Movies                                                          |");
                    Console.WriteLine($"|        4. Get all Movies                                                         |");
                    Console.WriteLine($"|        5. Get a movies review                                                    |");
                    Console.WriteLine($"|        6. Exit                                                                   |");
                    Console.WriteLine($"------------------------------------------------------------------------------------");
                    int choice = Inputter.validateNumInt("Enter your choice: ", 6);
                    var service = new MovieService(unit);
                    switch (choice)
                    {
                        case 1:
                            await service.AddMovie();
                            break;
                        case 2:
                            await service.UpdateMovie();
                            break;
                        case 3:
                            await service.DeleteMovie();
                            break;
                        case 4:
                            await service.ViewAllMovies();
                            break;
                        case 5:
                            await service.GetAllReviewers();
                            break;
                        case 6:
                            return;
                    }
                } while (!cont);
            }
        }
    }
}
