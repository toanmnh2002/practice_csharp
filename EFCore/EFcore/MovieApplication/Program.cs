﻿using Assignment.Util;
using MovieApplication.Entities;
using MovieApplication.Service;

namespace MovieApplication
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            bool check = true;
            do
            {
                Console.Clear();
                Console.WriteLine(" ------------------------");
                Console.WriteLine("|            Menu        |");
                Console.WriteLine("|                        |");
                Console.WriteLine("| 1. Movie               |");
                Console.WriteLine("| 2. Review              |");
                Console.WriteLine("| 3. Exit                |");
                Console.WriteLine("|                        |");
                Console.WriteLine(" ------------------------");
                int choice = Inputter.validateNumInt("Enter your choice: ", 3);
                switch (choice)
                {
                    case 1:
                        await MovieService.MovieMenu();
                        break;
                    case 2:
                        await ReviewService.ReviewMenu();
                        break;
                    default:
                        check = false;
                        break;
                }
            } while (check);
        }



    }

}