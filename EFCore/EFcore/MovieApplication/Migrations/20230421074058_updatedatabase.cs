﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace MovieApplication.Migrations
{
    /// <inheritdoc />
    public partial class updatedatabase : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: -2);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: -1);

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "Description", "Director", "ImageUrl", "Rating", "ReleaseDate", "Title" },
                values: new object[,]
                {
                    { 1, "Good", "ToanNguyen", "lovely", 5m, new DateTime(2023, 2, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), "Squirt Game" },
                    { 2, "hay", "chs", "vip", 5m, new DateTime(2023, 5, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "Herculess" }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "Description", "Director", "ImageUrl", "Rating", "ReleaseDate", "Title" },
                values: new object[,]
                {
                    { -2, "hay", "chs", "vip", 5m, new DateTime(2023, 5, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "Herculess" },
                    { -1, "Good", "ToanNguyen", "lovely", 5m, new DateTime(2023, 2, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), "Squirt Game" }
                });
        }
    }
}
