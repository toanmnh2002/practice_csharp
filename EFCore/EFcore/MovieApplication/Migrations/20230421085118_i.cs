﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace MovieApplication.Migrations
{
    /// <inheritdoc />
    public partial class i : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Reviews",
                keyColumn: "Id",
                keyValue: -2);

            migrationBuilder.DeleteData(
                table: "Reviews",
                keyColumn: "Id",
                keyValue: -1);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 2);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "Description", "Director", "ImageUrl", "Rating", "ReleaseDate", "Title" },
                values: new object[,]
                {
                    { 1, "Good", "ToanNguyen", "lovely", 5m, new DateTime(2023, 2, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), "Squirt Game" },
                    { 2, "hay", "chs", "vip", 5m, new DateTime(2023, 5, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "Herculess" }
                });

            migrationBuilder.InsertData(
                table: "Reviews",
                columns: new[] { "Id", "Content", "MovieId", "Rating", "Title" },
                values: new object[,]
                {
                    { -2, "I enjoyed this movie.", 2, 4, "Title" },
                    { -1, "This movie was amazing!", 1, 5, "helo" }
                });
        }
    }
}
