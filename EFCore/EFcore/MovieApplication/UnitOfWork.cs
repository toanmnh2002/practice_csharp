﻿using MovieApplication;
using MovieApplication.IRepository;
using MovieApplication.Repositories;
using MovieApplication.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieApplication
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDBcontext _appDBContext;
        private readonly IReviewRepository _reviewRepository;
        private readonly IMovieRepository _movieRepository;
        public UnitOfWork(AppDBcontext appDBContext)
        {
            _appDBContext = appDBContext;
            _reviewRepository = new ReviewRepository(_appDBContext);
            _movieRepository= new MovieRepository(_appDBContext);
        }

        public IReviewRepository ReviewRepository => _reviewRepository;

        public IMovieRepository MovieRepository => _movieRepository;

        public void Dispose()
        {
            _appDBContext.Dispose();
        }

        public async Task<int> SaveChangeAsync()
        {
            return await _appDBContext.SaveChangesAsync();
        }
    }
}
