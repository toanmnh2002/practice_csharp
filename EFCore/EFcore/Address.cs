﻿using System.ComponentModel.DataAnnotations;

public class Address
{
    [Key]
    public int AddressId { get; set; }
    [Required]
    public string City { get; set; }
}