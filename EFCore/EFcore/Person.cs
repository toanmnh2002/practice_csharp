﻿using System.ComponentModel.DataAnnotations;

public class Person
{
    [Key]
    public int Id { get; set; }
    [Required]
    public string Name { get; set; }

    public Person(int id, string name)
    {
        Id = id;
        Name = name;
    }

    public Person()
    {
    }
}
