﻿using System.ComponentModel.DataAnnotations.Schema;

public class DateOfBirth
{
    public int Id { get; set; }
    public int Age { get; set; }
    public DateTime date { get; set; }
}