﻿//int[] numbers = { 1, 2, 3, 4, 5 };
//var evenNumbers = numbers.Where(x => x % 2 == 0).Select(x => x * 2);

//int[] number1 = { 1, 2, 3, 4, 5 };
//var evenNumbers1 = from n in numbers
//                   where n % 2 == 0 || n == 1
//                   select n * 2;
//foreach (var item in evenNumbers)
//{
//    Console.Write(item + " ");
//}

//int[] number1 = { 1, 2, 3, 4, 5 };
//var evennum = from n
//              in number1
//              select n * 2;
//var even = number1.Select(x => x * 2);
//foreach (var n in evennum)
//{
//    Console.WriteLine(n);
//}
//foreach (var item in even)
//{
//    Console.WriteLine(item);
//}
//evennum.ToList().ForEach(x =>x.ToString());

var fruits = new List<string> { "apple", "banana", "orange", "pineapple", "mango" };
// 1. select & print fruit & make it upper. 
//var upper = from n in fruits select n.ToUpper();
//var upper = fruits.Select(x => x.ToUpper());
//foreach (var n in upper)
//{
//    Console.WriteLine(n);
//}
// 2. select & print fruit + length of fruit. example  => apple:5 
//var result = fruits.Select(x => $"{x}:{x.Length}");
//var result = from n in fruits select $"{n}:{n.Length}";
//foreach (var fruit in result)
//{
//    Console.WriteLine(fruit);
//}
// 3. select & print the first character of fruit 
//var result1 = fruits.Select(x => $"{x[0]}");
//var result1 = from n in fruits select $"{n[0]}";
//foreach (var item in result1)
//{
//    Console.WriteLine(item);
//}
// 4. select & print revert the fruit name: apple => elppa
//var result2 = fruits.Select(x => new string(x.Reverse().ToArray()));
//var result2=from n in fruits select new string(n.Reverse().ToArray());
//foreach (var item in result2)
//{
//    Console.WriteLine(item);
//}



