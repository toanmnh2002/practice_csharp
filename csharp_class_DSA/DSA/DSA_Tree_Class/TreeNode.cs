﻿using DSA_Tree_Class;

//var root = new TreeNode(1);
//var node2 = new TreeNode(2);
//var node7 = new TreeNode(7);
//var node71 = new TreeNode(71);
//var node13 = new TreeNode(13);
//root.left = node2;
//root.right = node7;
//node2.left = node71;
//node2.right = node13;

//Console.Write("preoder(root-left-right)\n");
//Traverse1(root);
//Console.Write("\ninorder(left-right-root)\n");
//Traverse2(root);
//Console.Write("\npostorder(left-root-right)\n");
//Traverse3(root);
var a = new TreeNode[5];
var root44 = new TreeNode(44);
var node11 = new TreeNode(11);
var node22 = new TreeNode(22);
root44.left = node11;
root44.right = node22;
root44.ChildrenNode = new TreeNode[2] { new TreeNode(44), new TreeNode(41) };
Traverse(root44);

void Traverse(TreeNode node)
{
    if (node is not null)
    {
        Console.WriteLine(node.value);
        if (node.ChildrenNode is not null)
        {
            foreach (var item in node.ChildrenNode)
            {
                Traverse(item);
            }
        }
    }
}
void Traverse2(TreeNode node)
{
    if (node is not null)
    {
        Traverse2(node.left);
        Console.Write(node.value + " ");
        Traverse2(node.right);
    }
}
void Traverse1(TreeNode node)
{
    if (node is not null)
    {
        Console.Write(node.value + " ");
        Traverse1(node.left);
        Traverse1(node.right);
    }
}
void Traverse3(TreeNode node)
{
    if (node is not null)
    {
        Traverse3(node.left);
        Traverse3(node.right);
        Console.Write(node.value + " ");
    }
}

namespace DSA_Tree_Class
{

    public class TreeNode
    {
        private TreeNode[] childrenNode;
        public int value { get; set; }
        public TreeNode right { get; set; }
        public TreeNode left { get; set; }
        public TreeNode[] ChildrenNode { get => childrenNode; set => childrenNode = value; }
        
        public TreeNode(int value)
        {
            this.value = value;
        }
    }

}
