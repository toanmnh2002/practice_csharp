﻿//static bool Search(int number, int[] a)
//{
//    foreach (int i in a.Reverse())
//    {
//        if (i == number)
//        {
//            return true;
//        }
//    }
//    return false;
//}

//if (Search(number, a))
//{
//    Console.WriteLine("found");
//}
//else
//{
//    Console.WriteLine("not found");
//}
//if (Search1(number, a))
//{
//    Console.WriteLine("found");
//}
//else
//{
//    Console.WriteLine("not found");
//}
//static bool Search1(int number, int[] a)
//{
//    for (int i = a.Length; i >= 0; i--)
//    {
//        if (i == number)
//        {
//            return true;
//        }
//    }
//    return false;
//}
//for (int i = a.Length-1; i >= 0; i--)
//{
//    Console.Write(a[i] + " ");
//}


static int BinarySearch(int number, int[] arr)
{
    int left = 0;
    int right = arr.Length - 1;

    while (left <= right)
    {
        int mid = (right + left) / 2;
        if (arr[mid] == number)
        {
            return mid;
        }
        else if (arr[mid] < number)
        {
            left = mid + 1;
        }
        else
        {
            right = mid - 1;
        }
    }
    return 0;
}
var arr = new int[] { 1, 2, 3, 4, 5 };
Console.Write("Enter number: ");
int number = int.Parse(Console.ReadLine());
int resut = BinarySearch(number, arr);
if (resut == 0)
{
    Console.WriteLine("not found");
}
else
{
    Console.WriteLine($"found {number} at index {resut} ");
}
//if (BinarySearch(6, arr))
//{
//    Console.WriteLine("Success");
//}
//else
//{
//    Console.WriteLine("fail");
//}
