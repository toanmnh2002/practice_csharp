﻿using Practice_Interface_7_10;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice_Inheritance_7_10
{
    public class Rectangle : IShape
    {
        public Rectangle()
        {
        }

        public Rectangle(float length, float width)
        {
            Length = length;
            Width = width;
        }

        public float Length { get; set; }
        public float Width { get; set; }

        public float CalculateArea()
        {
            return Length * Width;
        }

        public float CalculatePerimeter()
        {
            return 2 * (Length + Width);
        }
    }
}
