﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice_Interface_7_10
{
    public interface IShape
    {
         float CalculateArea();
         float CalculatePerimeter();
    }
}
