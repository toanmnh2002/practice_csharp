﻿//A b = new B();
//b.S();
//class A
//{
//    public virtual void S() => Console.WriteLine("froma");
//}//no data hiding }
//class B : A { public override void S() { Console.WriteLine("fromb"); } }//override in base class

//IA f = new IA();
//interface IA
//{

//}


//abstraction
//IA f = new F();
//f.M();
//interface IA { void M(); }
//class F : IA { public void M() => Console.WriteLine("hi"); }


//not abstraction
//IA f = new F(); f.M();
//interface IA
//{
//    virtual void M() => Console.WriteLine("hi");
//}
//class F : IA
//{
//    public void M() => Console.WriteLine("ha");
//}

//interface IInterface
//{
//    public virtual void A()
//    {
//        Console.WriteLine("hello");
//    }
//}

//interface IInterface
//{
//    public static void A()
//    { }
//}
//abstract class MyClass
//{
//    public static void A()
//    { }
//}
//abstract class s : MyClass
//{

//}

//IA f = new F(); f.M(); 
//interface IA { void M() => Console.WriteLine("hi"); }
//class F : IA
//{
//    public void M() => Console.WriteLine("ha");//override fantasy base class
//}

//object->class->interface

