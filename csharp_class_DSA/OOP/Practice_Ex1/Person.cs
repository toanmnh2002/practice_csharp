﻿var person1 = new Person("Toamn", "Nguyen", 20);
var person2 = new Person("Toaaan", "Manh", 22,"toanmsnh@gmail.com");
var person3 = new Person();
person3.firstName = "joao"; person3.lastName = "felix"; person3.age = 23;
person1.email = "Toanmnh2002@gmail.com";
person2.age= 30;
Console.WriteLine(person1.GetFullName());
Console.WriteLine(person1.GetContactInfo());
Console.WriteLine(person2.GetContactInfo(true));
Console.WriteLine(person3.GetFullName());
Console.WriteLine(person3.GetContactInfo(false));
class Person
{

    public string firstName { get; set; }
    public string lastName { get; set; }
    public int age { get; set; }
    public string email { get; set; }
    public Person(string firstName, string lastName, int age)
    {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    public Person()
    {
    }

    public Person(string firstName, string lastName, int age, string email)
    {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.email = email;
    }
    public string GetFullName()
    {
        return $"FullName:{firstName}{lastName}";
    }
    public string GetContactInfo()
    {
        return $"FullName:{firstName}{lastName};Email:<{email}>";
    }
    public string GetContactInfo(bool includeAge)
    {
        string contactInfo = $"FullName:{firstName}{lastName}";
        if (includeAge)
        {
            contactInfo += $";Age:{age}";
        }
        contactInfo += $";Email:<{email}>";
        return contactInfo;
    }
}