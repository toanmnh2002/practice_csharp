﻿//using Practice_Encapsulation;
//var libra = new Library();
//libra.AddBook("felix");
//libra.AddBook("havertz");
//libra.AddBook("sterling");


//libra.RemoveBook("felix");

//libra.AddBorrower("chelsea");
//libra.AddBorrower("MUDRYK");
//libra.RemoveBorrower("chelsea");
//Console.WriteLine($"Books:{libra.BookCount}");
//Console.WriteLine($"Borrows:{libra.BorrowCount}");
//libra.Print();




//namespace Practice_Encapsulation
//{
//    public class Library
//    {
//        private string[] Books;
//        private string[] Borrowers;

//        public int BookCount { get; private set; }
//        public int BorrowCount { get; private set; }
//        public Library()
//        {
//            Books = new string[10];
//            Borrowers = new string[10];
//        }
//        public void AddBook(string bookName)
//        {
//            if (!Books.Contains(bookName))
//            {
//                if (BookCount < Books.Length)
//                {
//                    Books[BookCount] = bookName;
//                    BookCount++;
//                    Console.WriteLine($"Added {bookName}");
//                }
//                else
//                {
//                    Console.WriteLine("Library is full!");
//                }
//            }
//            else
//            {
//                Console.WriteLine("Duplicate book!");
//            }

//        }
//        public void RemoveBook(string bookName)
//        {
//            int index = Array.IndexOf(Books, bookName);
//            if (index != -1)
//            {
//                Books[index] = null;
//                Console.WriteLine($"{bookName} has removed");
//            }
//            else
//            {
//                Console.WriteLine("fail!");
//            }
//        }

//        public void AddBorrower(string borrowerName)
//        {
//            if (!Borrowers.Contains(borrowerName))
//            {
//                if (BorrowCount < Borrowers.Length)
//                {
//                    Borrowers[BorrowCount] = borrowerName;
//                    BookCount++;
//                    Console.WriteLine($"Added {borrowerName}");
//                }
//                else
//                {
//                    Console.WriteLine("library is full");
//                }
//            }
//            else
//            {
//                Console.WriteLine("Duplicate Borrower");
//            }
//        }

//        public void RemoveBorrower(string borrowerName)
//        {
//            int indexToRemove = Array.IndexOf(Borrowers, borrowerName);
//            if (indexToRemove != -1)
//            {
//                Borrowers[indexToRemove] = null;
//                Console.WriteLine($"{borrowerName} has removed");
//            }
//            else
//            {
//                Console.WriteLine("fail!");
//            }
//        }
//        public void Print()
//        {
//            Console.WriteLine("Books List");
//            foreach (var item in this.Books)
//            {
//                Console.Write(item + " ");
//            }
//            Console.WriteLine("\nBorrowers List");
//            foreach (var item in this.Borrowers)
//            {
//                Console.Write(item + " ");
//            }
//        }
//    }
//}
