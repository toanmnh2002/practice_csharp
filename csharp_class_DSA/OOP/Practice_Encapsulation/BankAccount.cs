﻿//using Practice_Encapsulation;

//var toan = new BankAccount(1, "ToanNguyen", 0);
//Console.WriteLine($"Hello {toan.AccountHolder}! \nYour account id is {toan.AccountNumber}\nYour balance is {toan.AccountBalance}  ");
//if (toan.Deposit())
//{
//    Console.WriteLine($"Deposit Sucessfully! Account balance of {toan.AccountHolder} is " + toan.AccountBalance);
//}
//else
//{
//    Console.WriteLine("Deposit fail!");
//}
//if (toan.Withdraw())
//{
//    Console.WriteLine($"Withdraw Sucessfully! Account balance of {toan.AccountHolder} is " + toan.AccountBalance);

//}
//else
//{
//    Console.WriteLine("Deposit fail!");
//}
//namespace Practice_Encapsulation
//{
//    public class BankAccount
//    {

//        public int AccountNumber { get; set; }
//        public string AccountHolder { get; set; }
//        public decimal AccountBalance { get; private set; }

//        public BankAccount(int accountNumber, string accountHolder, decimal accountBalance)
//        {
//            AccountNumber = accountNumber;
//            AccountHolder = accountHolder;
//            AccountBalance = accountBalance >= 0 ? accountBalance : decimal.Parse("AccountBalance is not negative");
//        }
//        public bool Deposit()
//        {
//            decimal amount;
//            bool check;
//            do
//            {
//                Console.Write("Enter amount to deposit: ");
//                check = decimal.TryParse(Console.ReadLine(), out amount);
//                if (amount < 0)
//                {
//                    check = false;
//                }
//                if (check is false)
//                {
//                    Console.WriteLine("Invalid amount");
//                    return false;
//                }
//            } while (!check);
//            Console.WriteLine($"You entered {amount}$");
//            AccountBalance += amount;
//            return true;
//        }
//        public bool Withdraw()
//        {
//            decimal amount;
//            bool check;
//            do
//            {
//                Console.Write("Enter amount to withdraw: ");
//                check = decimal.TryParse(Console.ReadLine(), out amount);
//                if (amount < 0)
//                {
//                    check = false;
//                }
//                if (check is false)
//                {
//                    Console.WriteLine("Invalid amount");
//                    return false;
//                }
//            } while (!check);
//            if (CanAfford(amount))
//            {
//                AccountBalance -= amount;
//                return true;
//            }
//            else return false;
//        }
//        public bool CanAfford(decimal amount)
//        {
//            return AccountBalance >= amount;
//        }
//    }
//}
