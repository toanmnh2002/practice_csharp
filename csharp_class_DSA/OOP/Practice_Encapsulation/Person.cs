﻿//using Practice_Encapsulation;

//var person1 = new Person("toan",20);
//Console.WriteLine($"Name: {person1.Name}, Age: {person1.Age}");
//person1.Name = "John Smith";
//person1.Age = 30;

//// And print the updated values:
//Console.WriteLine($"Name: {person1.Name}, Age: {person1.Age}");

//namespace Practice_Encapsulation
//{
//    public class Person
//    {
//        private string name;
//        private int age;

//        public Person(string name, int age)
//        {
//            this.Name = name;
//            this.Age = age;
//        }
//        public string Name { get => name; set => name = value; }
//        public int Age { get => age; set => age = value; }
//    }
//}
