﻿//Create a class Person with:
//Fields for name and age.
//Write Properties(Name and Age) to get and set the values of the fields.
//Initialize an instance of the class with and set Properties.
//Print the value of those properties.
//Update the Properties & see the change apply by print it to console


//Create a class BankAccount with

//Field for AccountNumber, AccountHolder, and Balance.
//Write methods to Deposit and WithDraw methods from the account and ensuring that the balance cannot become negative.
//Initialize an instance of the class with account number, account holder, and balance parameters.
//Call the deposit method with an amount parameter to add funds to the account balance.
//Call the withdraw method with an amount parameter to remove funds from the account balance.
//Verify that the balance cannot become negative by checking the account balance property after a withdrawal.

//Create a class Employee with:

//Fields for Name and Salary.
//Write Properties or Methods to get and set the values of these properties.
//Initialize an instance of the class with a name and salary parameter.
//Call the get and set methods to retrieve and update the name and salary properties of the instance.

//Create a class Student with:

//fields for Name and GPA.
//Write Properties or  methods to get and set the values of these fields.
//Initialize an instance of the class with a name and GPA parameter.
//Call the get and set methods to retrieve and update the name and GPA properties of the instance.

//Create a class Library with:
//Field for Books(array with 10 string items to store book name) & Borrowers(array with 10 string items to store borrower name).
//Write public methods to add and remove books and borrowers, ensuring that no duplicates are added(you can not add the same book).
//Initialize an instance of the class.
//Call the add book method with a “book name” parameter to add a book to the library.
//Call the remove book method with a “book name” parameter to remove a book from the library.
//Call the add borrower method with a “borrower name” parameter to add a borrower to the library.
//Call the remove borrower method with a “borrower name” parameter to remove a borrower from the library.
//Verify that no duplicates are added by attempting to add the same book or borrower twice.
//checking that the count of books or borrowers through BookCount property & BrrowerCount property.
