﻿using Practice_Inheritance_7_10;

var shape = new Shape[]
{
    new Rectangle(5,10),new Circle(5),new Rectangle(40,59),new Circle(10)
};

foreach (var item in shape)
{
    Console.WriteLine($"{item.GetType().Name}\n" +
        $"Perimeter of this {item.GetType().Name} is {item.CalculatePerimeter()}\n" +
        $"Area of this {item.GetType().Name} is {item.CalculateArea()}");
}
namespace Practice_Inheritance_7_10
{
    public abstract class Shape
    {
        public virtual float CalculateArea() => 0;
        public virtual float CalculatePerimeter() => 0;
    }
}
