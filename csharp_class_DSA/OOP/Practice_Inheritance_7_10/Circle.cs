﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice_Inheritance_7_10
{
    public class Circle : Shape
    {
        private static readonly float PI_VALUE = 3.14f;
        public float Radius { get; set; }

        public Circle(float radius)
        {
            Radius = radius;
        }

        public override float CalculateArea()
        {
            return PI_VALUE * Radius * Radius;
        }

        public override float CalculatePerimeter()
        {
            return 2 * PI_VALUE * Radius;
        }
    }
}
