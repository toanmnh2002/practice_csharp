﻿namespace Practice_Inheritance_7_10
{
   public class Rectangle : Shape
    {
        public Rectangle(float length, float width)
        {
            Length = length;
            Width = width;
        }
        public float Length { get; private set; }
        public float Width { get; private set; }

        public virtual float CalculateArea()
        {
            return Length * Width;
        }

        public override float CalculatePerimeter()
        {
            return 2 * (Length + Width);
        }
    }
}
