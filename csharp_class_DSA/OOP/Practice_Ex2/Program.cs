﻿var product1 = new Product();
product1.name = "Toan";
product1.description = "toannnnguyen";
product1.price = 100;
product1.isAvailable = true;
//Console.WriteLine(product1.PrintInfo());
var product2 = new Product(1, "joao", "chelsea player", 5, true);
product2.isAvailable = false;
product2.description = "jack";
//Console.WriteLine(product2.PrintInfo());
var product3 = new Product(1, "enzo", "chelsea player", 5, true);
product3.isAvailable = false;
//Console.WriteLine(product3.PrintInfo(false));
//Console.WriteLine(product3.PrintInfo(true));
Console.WriteLine("List product~~~~~~~~~~~~~~~~");
var list = new List<Product>();
list.Add(product1);
list.Add(product2);
list.Add(product3);

//foreach (var item in list)
//{
//    Console.WriteLine(item.PrintInfo());
//}
string filePath = "D:\\hoctap\\C#Class\\PracticeFile\\Product.txt";
var product = new Product();
bool success = product.WriteListToFile(list, "Product");
if (success)
{
    Console.WriteLine("Write operation successful.");
}
else
{
    Console.WriteLine("Write operation failed.");
}
Console.WriteLine("Read from file");
var a = product.ReadFromFile("Product");
foreach (var item in a)
{
    Console.WriteLine(item.PrintInfo());
}
class Product
{
    public Product()
    {
    }
    public Product(int id, string name, string description, decimal price)
    {
        Id = id;
        this.name = name;
        this.description = description;
        this.price = price;
        isAvailable = true;
    }

    public Product(int id, string name, string description, decimal price, bool isAvailable)
    {
        Id = id;
        this.name = name;
        this.description = description;
        this.price = price;
        this.isAvailable = isAvailable;
    }

    public int Id { get; set; }
    public string name { get; set; }
    public string description { get; set; }
    public decimal price { get; set; }
    public bool isAvailable { get; set; }

    public string PrintInfo()
    {
        return $"Id:{Id},Name:{name},Description:{description},Price:{price},IsAvailable:{isAvailable}";
    }
    public string PrintInfo(bool check)
    {
        string b = $"Id:{Id},Name:{name},Price:{price}";
        if (check)
        {
            b += $";Description:{description}";
        }
        b += $",IsAvailable:{isAvailable}";
        return b;
    }// This method writes a list of Product objects to a text file located at the specified path with the given fileName.
    public bool WriteListToFile(List<Product> productList, string fileName)
    {
        // The filePath variable contains the path to the file where the product list is going to be saved.
        string filePath = $"D:\\hoctap\\C#Class\\PracticeFile\\{fileName}.txt";

        // Checks if the file already exists and deletes it if it does.
        if (File.Exists(filePath))
        {
            File.Delete(filePath);
        }

        // Writes each product in the list to a new line in the file using StreamWriter.
        using (StreamWriter writer = new StreamWriter(filePath))
        {
            foreach (Product product in productList)
            {
                // Formats the data into a string that will be saved in the .txt file. Each field is separated by '|'.
                string fileContent = $"{product.Id}|{product.name}|{product.description}|{product.price}|{product.isAvailable}";
                writer.WriteLine(fileContent);
            }
        }

        // Returns true to indicate that the writing process was successful.
        return true;
    }

    // This method reads a list of Product objects from a text file located at the specified path with the given fileName.
    public List<Product> ReadFromFile(string fileName)
    {
        // The filePath variable contains the path to the file which contains the saved product list.
        string filePath = $"D:\\hoctap\\C#Class\\PracticeFile\\{fileName}.txt";

        // A new empty list of Product objects is created here.
        var productList = new List<Product>();

        // The content of the file is read and stored in the fileContent array.
        string[] fileContent = File.ReadAllLines(filePath);

        // Iterates through each line of the fileContent array, splits each line by '|' delimiter, 
        //and assigns their respective values to a new instance of the Product class.
        foreach (string line in fileContent)
        {
            string[] productFields = line.Split("|");
            int id = int.Parse(productFields[0]);
            string name = productFields[1];
            string description = productFields[2];
            decimal price = decimal.Parse(productFields[3]);
            bool isAvailable = bool.Parse(productFields[4]);

            // Creates a new instance of the Product class with the extracted values and adds it to the productList.
            Product product = new Product(id, name, description, price, isAvailable);
            productList.Add(product);
        }

        // Returns the populated productList object.
        return productList;
    }



}