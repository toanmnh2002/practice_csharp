﻿using Practice_Encapsulation_2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
Library l = new Library();
l.RemoveBook("hello");
l.Print();
namespace Practice_Encapsulation_2
{
    public class Library
    {
        private List<string> Books = new List<string>();
        private List<string> Borrows = new List<string>();
        public void AddAccount(string BookName)
        {
            if (!Books.Contains(BookName))
            {
                if (Books.Count() < 10)
                {
                    Books.Add(BookName);
                    Console.WriteLine($"Added {BookName}");
                }
                else
                {
                    Console.WriteLine("Library is full");
                }
            }
            else
            {
                Console.WriteLine("fail!");
            }
        }
        public void RemoveBook(string BookName)
        {
            if (Books.Count() > 0)
            {
                Books.Remove(BookName);
                Console.WriteLine($"{BookName} has removed");
            }
            else
            {
                Console.WriteLine("Nothing to remove");
            }
        }


        public void AddCustomer(string Borrow)
        {
            if (!Borrow.Contains(Borrow))
            {
                if (Books.Count() < 10)
                {
                    Books.Add(Borrow);
                    Console.WriteLine($"Added {Borrow}");
                }
                else
                {
                    Console.WriteLine("Library is full");
                }
            }
            else
            {
                Console.WriteLine("fail!");
            }
        }
        public void RemoveCustomer(string Borrow)
        {
            if (!Borrows.Contains(Borrow))
            {
                if (Borrows.Count() < 10)
                {
                    Borrows.Remove(Borrow);
                    Console.WriteLine($"{Borrow} has removed");
                }
                else
                {
                    Console.WriteLine("Library is full");
                }
            }
            else
            {
                Console.WriteLine("Duplicate book");
            }
        }
        public void Print()
        {
            foreach (var item in Books)
            {
                Console.WriteLine(item + " ");
            }
        }
    }
}
