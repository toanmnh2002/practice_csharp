﻿//A a = new A();
//a.a = 1000;
//A b = a;
//b.S();
//A c = new A();
//Console.WriteLine(ReferenceEquals(a, b));
//Console.WriteLine(ReferenceEquals(b, c));
//Console.WriteLine(ReferenceEquals(a, c));
//class A
//{
//    public int a = 1;
//    public void S() => Console.WriteLine(a);
//}
using Encapsulation;

class Program
{
    static void Main(String[] args)
    {
        Console.WriteLine("CAR");
        var car=new Car("Bmw","honda",2002,23,232);
        car.GetInfo();
        Console.WriteLine();
        Console.WriteLine("Motorbike");
        var motor = new Motorcycle("mercedes","mer",2002,23,22222);
        motor.GetInfo();
        Console.WriteLine();
    }
}

