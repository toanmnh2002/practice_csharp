﻿namespace Encapsulation
{
    public class Vehicle
    {
        public string make { get; set; }
        public string model { get; set; }
        public int year { get; set; }
        public int numWheels { get; set; }

        public Vehicle()
        {
        }

        public Vehicle(string make, string model, int year, int numWheels)
        {
            this.make = make;
            this.model = model;
            this.year = year;
            this.numWheels = numWheels;
        }

        public virtual string? GetInfo()
        {
            return $"Make:{make}\tModel:{model}\tYear:{year}\tnumWheels:{numWheels}";
        }
    }
}
