﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Threading.Tasks;

namespace Practice_Inheritance
{
    public class Cat : Animal
    {
        public string FurColor { get; set; }

        public Cat(string furColor)
        {
            FurColor = furColor;
        }

        public Cat()
        {
        }

        public Cat(string name, string type, bool gender, string furColor) : base(name, type, gender)
        {
            FurColor = furColor;
        }
    }
}
