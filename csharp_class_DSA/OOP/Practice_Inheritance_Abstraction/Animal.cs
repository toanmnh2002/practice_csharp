﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace Practice_Inheritance_Abstraction
//{
//    public abstract class Animal
//    {
//        public virtual void MakeSound() => Console.WriteLine("aa");
//    }
//    class Dog : Animal
//    {
//        public override void MakeSound()
//        {
//            Console.WriteLine("GauGAU");
//        }
//    }
//    class Cat : Animal
//    {
//        public virtual void MakeSound()
//        {
//            Console.WriteLine("memeo");
//        }
//    }
//    class Bird : Animal
//    {
//        public override void MakeSound()
//        {
//            Console.WriteLine("okko");
//        }
//    }
//    class program
//    {
//        static void Main()
//        {
//            Bird b=new Bird();
//            b.MakeSound();
//        }
//    }
//}
