﻿

using OOP;
using System.Security.Cryptography;

namespace OOP
{
    public class Student1
    {
        public Student1(string name, int age, double gPA)
        {
            Name = name;
            Age = age;
            GPA = gPA;
        }

        public Student1()
        {
        }

        public string Name { get; set; }
        public int Age { get; set; }
        public double GPA { get; set; }

        public string GetStudentInfo()
        {
            return $"Name: {Name}, Age: {Age}, GPA: {GPA}";
        }
        Student1 s1=new Student1("toan",4,4);


    }
}

