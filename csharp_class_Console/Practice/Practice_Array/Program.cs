﻿//exercise for practicing with arrays
//Declare an array of integers with values 1, 2, 3, 4, 5.Print the values in the array to the console.Expected output: 1 2 3 4 5


//int[] arr = {1,2,3,4,5};
//foreach (var item in arr)
//{
//    Console.Write(item+" "); 
//}



//Declare an array of strings with values "apple", "banana", "cherry", "date", "elderberry".Print the values in the array to the console.Expected output: apple banana cherry date elderberry
//string[] arr = { "apple", "banana", "cherry", "date", "elderberry" };
//foreach (string str in arr)
//{
//    Console.Write(str+" ");
//}

//Declare an array of integers with values 5.Use a loop to add 2 to each value in the array, and then print the values to the console.Expected output: 3 4 5 6 7


//int[] arr = { 1, 2, 3, 4, 5 };
//foreach (var item in arr)
//{
//    Console.Write(item + 2 + " ");
//}
//for (int i = 0; i < arr.Length; i++)
//{
//    Console.Write(arr[i]+2 + " ");
//}

//Declare an array of integers with values 1, 2, 3, 4, 5.Use a loop to calculate the sum of the values in the array, and then print the sum to the console.Expected output: 15
//int[] a = { 1, 2, 3, 4, 5 };
//int count = 0, sum = 0;
//foreach (var item in a)
//{
//    sum += item;
//    count++;
//}
//Console.Write(sum); 
//Declare an array of integers with values 1, 2, 3, 4, 5.Use a loop to calculate the average of the values in the array, and then print the average to the console.Expected output: 3

using System;

int[] a = { 1, 2, 3, 4, 5 };
//double Avg(int[] a)
//{
//    int sum = 0, count = 0;
//    foreach (var item in a)
//	{
//		sum += item;
//		count ++;
//	}
//	return (double)sum/a.Length;
//}
//Console.WriteLine($"Average is {Avg(a)}");

//Declare an array of integers with values 1, 2, 3, 4, 5.Use a loop to find the maximum value in the array, and then print the maximum value to the console.Expected output: 5
//Console.WriteLine(a.Max());
//Array.Sort(a);
//int FindMax(int[] n)
//{
//    for (int i = 0; i < a.Length; i++)
//    {
//        if (a[0] < a[i])
//        {
//            a[0] = a[i];
//        }
//    }
//    return a[0];
//}
//Console.WriteLine(FindMax(a)); 

//Declare an array of integers with values 1, 2, 3, 4, 5.Use a loop to find the minimum value in the array, and then print the minimum value to the console.Expected output: 1
//int FindMin(int[] n)
//{
//    for (int i = 0; i < a.Length; i++)
//    {
//        if (a[0] > a[i])
//        {
//            a[0] = a[i];
//        }
//    }
//    return a[0];
//}
//Console.WriteLine(FindMin(a)); 

//Declare an array of integers with values 1, 2, 3, 4, 5.Use a loop to sort the values in the array in descending order, and then print the sorted values to the console.Expected output: 5 4 3 2 1
//foreach (int i in a.Reverse())
//{
//    Console.Write(i + " ");
//}

//Declare an array of integers with values 1, 2, 3, 4, 5.Use a loop to reverse the order of the values in the array, and then print the reversed values to the console.Expected output: 5 4 3 2 1
//foreach (int i in a.Reverse())
//{
//    Console.Write(i + " ");
//}

//Declare an array of integers with values 1, 2, 3, 4, 5.Use a loop to search for the value 3 in the array,
//and then print "found" to the console if the value is found,
//and "not found" if the value is not found.Expected output: found
//Console.Write("Enter number : ");
//int n = int.Parse(Console.ReadLine());
//bool check = false;
//foreach (var item in a)
//{
//    if (n == item)
//    {
//        check = true;
//        break;
//    }
//}
//if (check)
//{
//    Console.WriteLine("found");
//}
//else
//{
//    Console.WriteLine("Not found");
//}
//Declare an array of integers with values 1, 2, 3, 4, 5. Use a loop to search for the value 6 in the array, and then print "found" to the console if the value is found, and "not found" if the value is not found. Expected output: not found

//Console.Write("Enter number : ");
//int n = int.Parse(Console.ReadLine());
//bool check = false;
//foreach (var item in a)
//{
//    if (n == item)
//    {
//        check = true;
//        break;
//    }
//}
//if (check)
//{
//    Console.WriteLine("found");
//}
//else
//{
//    Console.WriteLine("Not found");
//}
//Declare an array of strings with values "apple", "banana", "cherry", "date", "elderberry".
//Use a loop to concatenate all the strings in the array into a single string,
//separated by spaces, and then print the concatenated string to the console.
//Expected output: "apple banana cherry date elderberry"
string[] arr = { "apple", "banana", "cherry", "date", "elderberry" };
string check = "";
int count = 0;
foreach (string s in arr)
{
    check += s;
    if (count < arr.Length - 1) // add a space after all elements except the last one
    {
        check += " ";
    }
    count++;
}
Console.WriteLine(check);
