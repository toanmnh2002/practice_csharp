﻿using System.Collections.Generic;

//Find the length of a string:
Console.WriteLine();
Console.WriteLine("Toan".Length);

//String: "Hello, world!"
//Length: 13
Console.WriteLine();
Console.WriteLine("Hello, world!".Length);
//Convert a string to uppercase:

//String: "Hello, world!"
//Uppercase: "HELLO, WORLD!"
Console.WriteLine();
Console.WriteLine("Hello, world!".ToUpper());
//Convert a string to lowercase:
//String: "Hello, world!"
//Lowercase: "hello, world!"
Console.WriteLine();
Console.WriteLine("Hello, world!".ToLower());
//Trim whitespace from the beginning and end of a string:

//String: " Hello, world! "
//Trimmed: "Hello, world!"
Console.WriteLine();
Console.WriteLine(" Hello, world! ".Trim());
//Concatenate two strings:

//String 1: "Hello, "
//String 2: "world!"
//Concatenated: "Hello, world!"
Console.WriteLine();
var s1 = "Hello, ";
var s2 = "world!";
Console.WriteLine(string.Concat(s1,s2));

//Get a substring of a string:

//String: "Hello, world!"
//Substring: "world!"
Console.WriteLine();
Console.WriteLine("Hello, world!".Substring(6, 7));

//Replace a substring within a string:

//String: "Hello, world!"
//Replace "world" with "you": "Hello, you!"
Console.WriteLine();
Console.WriteLine("Hello, world!".Replace("world", "you"));
//Insert a substring into a string:

//String: "Hello, !"
//Insert "world" at position 7: "Hello, world!"
Console.WriteLine();
Console.WriteLine("Hello, !".Insert(7, "world"));
//Split a string into an array of substrings:

//String: "Hello, world!"
//Array: ["Hello,", "world!"]
Console.WriteLine();
string s = "Hello, world!";
string[] arr = s.Split(", ");
foreach (string item in arr)
{
    Console.Write(item);
}
//Find the index of a substring within a string:

//String: "Hello, world!"
//Index of "world": 7
Console.WriteLine();
Console.WriteLine(s.IndexOf("world"));
//Find the last index of a substring within a string:

//String: "Hello, world!"
//Last index of "l": 9
Console.WriteLine();
Console.WriteLine(s.LastIndexOf("l"));
//Check if a string starts with a given substring:

//String: "Hello, world!"
//Starts with "Hello": true
Console.WriteLine();
Console.WriteLine(s.StartsWith("Hello"));
//Check if a string ends with a given substring:

//String: "Hello, world!"
//Ends with "world!": true
Console.WriteLine();
Console.WriteLine(s.EndsWith("world!"));
//Check if a string contains a given substring:

//String: "Hello, world!"
//Contains "world": true
Console.WriteLine();
Console.WriteLine(s.Contains("world"));
