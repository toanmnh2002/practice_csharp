﻿using DSA_TreeSample;
using System.Xml.Linq;

public class Tree
{
    private TreeNode root;

    public void Insert(int item)
    {
        root = Insert(root, item);
    }
    private TreeNode Insert(TreeNode node, int item)
    {
        if (node == null)
        {
            node = new TreeNode(item);
        }
        else if (item < node.Value)
        {
            node.Left = Insert(node.Left, item);
        }
        else if (item > node.Value)
        {
            node.Right = Insert(node.Right, item);
        }

        return node;
    }

    public bool Contains(int item)
    {
        return Contains(root, item);
    }
    TreeNode search(TreeNode p, int x)
    {
        if (p == null) return (null);
        if (p.Value == x) return (p);
        if (x < p.Value)
            return (search(p.Left, x));
        else
            return (search(p.Right, x));
    }

    private bool Contains(TreeNode node, int item)
    {
        if (node == null)
        {
            return false;
        }
        else if (item == node.Value)
        {
            return true;
        }
        else if (item < node.Value)
        {
            return Contains(node.Left, item);
        }
        else // item > node.Value
        {
            return Contains(node.Right, item);
        }
    }

    public void Traverse()
    {
        Traverse(root);
    }

    private void Traverse(TreeNode node)
    {
        if (node != null)
        {
            Traverse(node.Left);
            Console.Write(node.Value + " ");
            Traverse(node.Right);
        }
    }
}

public class Program
{
    public static void Main(string[] args)
    {
        Tree tree = new Tree();

        // Prompt the user to enter a list of integers to add to the tree
        Console.WriteLine("Enter a list of integers (separated by spaces) to add to the tree:");
        string[] input = Console.ReadLine().Split(' ');
        foreach (string s in input)
        {
            int n = int.Parse(s);
            tree.Insert(n);
        }

        // Prompt the user to enter a number to search for in the tree
        Console.WriteLine("Enter a number to search for in the tree:");
        int search = int.Parse(Console.ReadLine());
        Console.WriteLine("Contains " + search + ": " + tree.Contains(search));

        // Traverse the tree and print the items to the console
        Console.Write("Traverse: ");
        tree.Traverse();
        Console.WriteLine();
    }
}