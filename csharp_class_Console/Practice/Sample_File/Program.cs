﻿string inputFile = "D:\\hoctap\\C#Class\\PracticeFile\\Student.txt";
string outputFile = "averages.txt";

// Override default file names if provided as command-line arguments
if (args.Length >= 1) inputFile = args[0];
//if (args.Length >= 2) outputFile = args[1];

// Attempt to read grades from input file
int[][] grades;
try
{
    grades = ReadGrades(inputFile);
}
catch (Exception ex)
{
    Console.WriteLine($"Error: {ex.Message}");
    return;
}

// Calculate averages and write to output file
//try
//{
//    WriteAverages(outputFile, grades);
//}
//catch (Exception ex)
//{
//    Console.WriteLine($"Error: {ex.Message}");
//}


static int[][] ReadGrades(string fileName)
{
    string[] lines = File.ReadAllLines(fileName);

    // Check for empty file
    if (lines.Length == 0)
    {
        throw new Exception("File is empty.");
    }

    int[][] grades = new int[lines.Length][];
    for (int i = 0; i < lines.Length; i++)
    {
        string[] fields = lines[i].Split(':');
        if (fields.Length != 2)
        {
            throw new Exception($"Invalid data on line {i + 1}.");
        }

        string[] gradeFields = fields[1].Split();
        int[] row = new int[gradeFields.Length];
        for (int j = 0; j < gradeFields.Length; j++)
        {
            if (!int.TryParse(gradeFields[j], out row[j]))
            {
                throw new Exception($"Invalid grade on line {i + 1}, column {j + 1}.");
            }
        }

        grades[i] = row;
    }

    return grades;
}

//static void WriteAverages(string fileName, int[][] grades)
//{
//    using (StreamWriter writer = new StreamWriter(fileName))
//    {
//        for (int i = 0; i < grades.Length; i++)
//        {
//            double average = 0;
//            for (int j = 0; j < grades[i].Length; j++)
//            {
//                average += grades[i][j];
//            }
//            average /= grades[i].Length;

//            writer.WriteLine($"{GetStudentName(i)}: {average:F1}");
//            Console.WriteLine($"{GetStudentName(i)}: {average:F1}");
//        }
//    }
//}

static string GetStudentName(int index)
{
    switch (index)
    {
        case 0:
            return "Alice";
        case 1:
            return "Bob";
        case 2:
            return "Charlie";
        default:
            throw new Exception("Invalid student index.");
    }
}