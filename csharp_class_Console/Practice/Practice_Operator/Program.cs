﻿//1. Declare two integer variables and assign values to them.
//Perform the following arithmetic operations on the variables using the arithmetic operators: addition (+), subtraction (-), multiplication (*), division (/), and modulus (%). Print out the result of each operation.
//int a = 5, b = 8;
//Console.WriteLine("a+b={0}+{1}={2}", a, b, a + b);
//Console.WriteLine($"a-b={a}-{b}={a - b}");
//Console.WriteLine($"a*b={a}*{b}={a * b}");
//Console.WriteLine($"a/b={a}/{b}={a / b}");
//Console.WriteLine($"a%b={a}%{b}={a % b}");
//2. Use the increment (++) and decrement (--) operators to increase and decrease the value of one of the variables. Print out the value of the variable before and after using the operator.
//var i = 5;
//Console.WriteLine("\n" + i);
//i++;
//Console.WriteLine(++i);
//Console.WriteLine(++i);
//Console.WriteLine(++i);
//Console.WriteLine(i);
//++i;
//Console.WriteLine(++i);

//i--;
//Console.WriteLine(--i);
//Console.WriteLine(--i);
//Console.WriteLine(--i);
//Console.WriteLine(i);
//--i;
//Console.WriteLine(--i);

//3. Compare the two variables using the comparison operators (==, <, >, <=, >=, !=). Print out the result of each comparison.

int a1 = Random.Shared.Next(1, 100);
int b1 = Random.Shared.Next(1, 100);
Console.WriteLine($"a1={a1}\nb1={b1}");
Console.WriteLine($"a1 == b1->{a1 == b1}");
Console.WriteLine($"a1> b1->{a1 > b1}");
Console.WriteLine($"a1 < b1->{a1 < b1}");
Console.WriteLine($"a1 <= b1->{a1 <= b1}");
Console.WriteLine($"a1 >= b1->{a1 >= b1}");
Console.WriteLine($"a1 != b1->{a1 != b1}");


//4. Use the logical operators (!, &&, ||) to perform some logical operations on the variables. Print out the result of each operation.
//Use the assignment operators (=, +=, -=, *=, /=, %=) to modify the values of the variables. Print out the value of the variables before and after using each operator.
int a2 = Random.Shared.Next(1, 100);
int b2 = Random.Shared.Next(1, 100);
bool c = true;
bool d = false;

// Logical operators
Console.WriteLine(!c);        
Console.WriteLine(c && d);    
Console.WriteLine(c || d);    

// Assignment operators
Console.WriteLine(a2);        
a2 += 3;
Console.WriteLine(a2);       
a2 -= 2;
Console.WriteLine(a2);        
a2 *= 2;
Console.WriteLine(a2);        
a2 /= 3;
Console.WriteLine(a2);        
a2 %= 3;
Console.WriteLine(a2);        