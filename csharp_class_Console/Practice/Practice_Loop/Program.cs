﻿
////Flow - exercise - if-else -switchcase - loop.

////Generate a random number between 1 and 100, and write an if statement that checks if the number is greater than 50. If it is, print "The number is greater than 50"; otherwise, print "The number is less than or equal to 50".
//var num = Random.Shared.Next(1, 101);
//if (num > 50)
//{
//    Console.WriteLine("The number is greater than 50");
//}
//else
//{
//    Console.WriteLine("The number is less than or equal to 50");
//}
////Write an if-else statement that checks if a number is even or odd. If it's even, print "The number is even"; otherwise, print "The number is odd".
//var num1 = Random.Shared.Next(1, 100);
//if (num1 % 2 == 2)
//{
//    Console.WriteLine("The number is even");
//}
//else
//{
//    Console.WriteLine("The number is odd");

//}

////Write an if-else statement that checks a student's grade and prints a message depending on the grade. If the grade is greater than or equal to 90, print "Excellent work!"; if it's between 80 and 89, print "Good job!"; if it's between 70 and 79, print "Nice try!"; and if it's less than 70, print "You need more practice!".
//var grade = Random.Shared.Next(1, 100);
//if (grade >= 90)
//{
//    Console.WriteLine("Excellent work!");
//}
//else if (grade > 80 && grade < 89)
//{
//    Console.WriteLine("Good job!");
//}
//else if (grade > 70 && grade < 79)
//{
//    Console.WriteLine("Nice try!");
//}
//else
//{
//    Console.WriteLine("You need more practice!");
//}

////Write a switch statement that checks a person's job title and prints a message depending on the title. If the title is "Manager", print "You are in charge"; if it's "Employee", print "You work for the manager"; and if it's "Intern", print "You are learning".
using System.Security.Principal;

Console.Write("Enter title: ");
string role=Console.ReadLine();
Console.WriteLine(Get(role)); 
static string Get(string role)
{
    return role switch
    {
        "Manager" => "You are in charge",
        "Employee" => "You work for the manager",
        "Intern" => "You are learning",
        _ => "ngu"
    };
}

//Console.WriteLine(message);
////Write a loop that generates a random number between 1 and 10, and prints the number until it generates the number 7.
//var random = 0;
//do
//{
//    random = Random.Shared.Next(1, 11);
//    Console.WriteLine(random);
//} while (random != 7);

//Write a do-while loop that asks the user to enter a number between 1 and 100, and keeps asking until the user enters a valid number.
//using System;

//int number;
//bool check;
//do
//{
//    Console.Write("Enter num: ");
//    check = int.TryParse(Console.ReadLine(), out number);
//    if (check && number < 1 && number > 100)
//    {
//        break;
//    }
//    Console.WriteLine("Invalid number");
//} while (!check);
//Write a while loop that counts from 1 to 10 and prints each number.
//int i=1;
//while (i<=10)
//{
//    Console.Write(" "+i+" ");
//    i++;
//}

//Write a loop that generates 10 random numbers between 1 and 100, and prints the sum of the numbers.
//int sum = 0;
//int i = 0;
//while (i<10)
//{
//    var num = Random.Shared.Next(1, 101);
//    sum += num;
//    Console.WriteLine("Random number " + (i + 1) + ": " + num);
//    i++;
//}
//Console.WriteLine($"sum is {sum}" );
//Write a loop that prints the first 10 even numbers.
//int i = 2;
//while (i <= 20)
//{
//    Console.Write(i+" ");
//    i += 2;
//}

//Write a loop that generates a random number between 1 and 100, and keeps generating numbers until it generates a number that is greater than 50.
//int i = 0;
//while (i <= 50)
//{
//    i = Random.Shared.Next(1, 101);
//    Console.Write(i + " ");
//}

//Write an if statement that checks if a number is positive or negative, and prints a message accordingly.
//var num=Random.Shared.Next(-100,100);
//if (num < 0)
//{
//    Console.WriteLine("negative number");
//}
//else
//{
//    Console.WriteLine("positive number");
//}
//Write a switch statement that checks the day of the week and prints a message depending on the day. If it's Monday, print "It's Monday, the week is starting"; if it's Friday, print "It's Friday, the weekend is almost here"; and if it's any other day, print "It's a regular day".
//string day = "";
//while (day != "Monday" && day != "Tuesday" && day != "Wednesday" && day != "Thursday" && day != "Friday" && day != "Saturday" && day != "Sunday")
//{
//    Console.Write("Enter day: ");
//    day = Console.ReadLine();
//}
//var message = day switch
//{
//    "Monday" => "It's Monday, the week is starting",
//    "Tuesday" => "It's Friday, the weekend is almost here",
//    "Wednesday" => "It's a regular day",
//    "Thursday" => "It's a regular day",
//    "Friday" => "It's a regular day",
//    "Saturday" => "It's weekend",
//    "Sunday" => "It's weekend"
//};
//Console.WriteLine(message);
//Write an if-else statement that checks if a number is divisible by 3 and/or 5, and prints a message accordingly. If it's divisible by 3, print "The number is divisible by 3"; if it's divisible by 5, print "The number is divisible by 5"; and if it's divisible by both, print "The number is divisible by both 3 and 5".
//var num = Random.Shared.Next(1, 101);
//if (num % 3 == 0)
//{
//    Console.WriteLine("The number is divisible by 3");
//}
//else if (num % 5 == 0)
//{
//    Console.WriteLine("The number is divisible by 5");
//}
//else if (num % 3 == 0 && num % 5 == 0)
//{
//    Console.WriteLine("The number is divisible by both 3 and 5");
//}
//Write an expression that calculates the area of a rectangle with a length of 5 and a width of 10.
//Console.WriteLine($"the area of a rectangle with a length of 5 and a width of 10:{5*10}");

//Write a loop that prints the first 10 numbers in the Fibonacci sequence.
//int a = 0, b = 1, c = 0, count = 0;
//while (count<8)
//{
//    c = a + b;
//    Console.WriteLine(c);
//    a = b;
//    b = c;
//    count++;
//}

//Write an if-else statement that checks if a number is between 0 and 100, and prints a message accordingly. If it's between 0 and 50, print "The number is between 0 and 50"; if it's between 51 and 100, print "The number is between 51 and 100"; and if it's not between 0 and 100, print "The number is not between 0 and 100".
//var random = Random.Shared.Next(1, 101);
//if (random > 0 && random < 50)
//{
//    Console.WriteLine("between 0 and 50");
//}
//else if (random > 51 && random < 100)
//{
//    ;
//}
//else
//{
//    Console.WriteLine("The number is not between 0 and 100");
//}
//Write a loop that generates a random number between 1 and 100, and prints the number until it generates a number that is
//var random1 = Random.Shared.Next(1, 101);
//while (true)
//{
//    var random2 = Random.Shared.Next(1, 101);
//    Console.WriteLine(random2);
//    if (random1==random2)
//    {
//        break;
//    }
//}



