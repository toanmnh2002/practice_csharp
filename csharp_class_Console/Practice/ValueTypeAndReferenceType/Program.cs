﻿using System;
class Program
{
    public static void Main(string[] args)
    {
        void S(A a, B b)
        {
            a.a = 1000;
            b.b = 1000;
        }
        var a = new A(1);
        var b = new B(1);
        S(a, b);
        Console.WriteLine("a: " + a.a);
        Console.WriteLine("b: " + b.b);
    }
}

public class A
{
    public A(int x) => a = x; public int a;
}
public struct B
{
    public B(int x) => b = x; public int b;
}

