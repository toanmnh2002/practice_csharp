﻿//tutorial


//Console.Write("Enter the number of rows in the array:");
//int rows = int.Parse(Console.ReadLine());
//Console.Write("Enter the number of columns in the array:");
//int cols = int.Parse(Console.ReadLine());
//var arr=new int[rows, cols];
//for (int i = 0; i < rows; i++)
//{
//    for (int j = 0; j < cols; j++)
//    {
//        Console.Write("Enter the elements of the array:");
//        arr[i, j] = int.Parse(Console.ReadLine());
//    }
//}
//Console.WriteLine("The array is:");
//for (int i = 0; i < rows; i++)
//{
//    for (int j = 0; j < cols; j++)
//    {
//        Console.Write(arr[i, j] + " ");
//    }
//    Console.WriteLine();
//}

//// Step 2: Print all elements in array to console
//Console.WriteLine("Elements of the array:");
//for (int i = 0; i < rows; i++)
//{
//    for (int j = 0; j < cols; j++)
//    {
//        Console.Write(arr[i, j] + " ");
//    }
//    Console.WriteLine();
//}

//// Step 3: Print all row & the element that index are even index(0, 2, 4, etc...)
//Console.WriteLine("Elements in even-indexed rows:");
//for (int i = 1; i < rows; i += 2)
//{
//    for (int j = 0; j < cols; j++)
//    {
//        Console.Write(arr[i, j] + " ");
//    }
//    Console.WriteLine();
//}

//// Step 4: Sum of all elements in array
//int sum = 0;
//for (int i = 0; i < rows; i++)
//{
//    for (int j = 0; j < cols; j++)
//    {
//        sum += arr[i, j];
//    }
//}
//Console.WriteLine("sum: "+sum);
//Console.WriteLine("Sum of all elements in the array: " + sum);

//// Step 5: Get the min/max index in the array
//int index0 = 0, index1 = 0, index2 = 0, index3 = 0;
//int min = arr[0, 0], max = arr[0, 0];

//for (int i = 0; i < rows; i++)
//{
//    for (int j = 0; j < cols; j++)
//    {
//        if (arr[i, j] < min)
//        {
//            min = arr[i, j];
//            index0 = i;
//            index1 = j;
//        }
//        if (arr[i, j] > max)
//        {
//            max = arr[i, j];
//            index2 = i;
//            index3 = j;
//        }
//    }
//}

//Console.WriteLine("Minimum value in the array: " + min + ", at index [" + index0 + ", " + index1 + "]");
//Console.WriteLine("Maximum value in the array: " + max + ", at index [" + index2 + ", " + index3 + "]");

//1.Write a program that initializes a 2D array of integers with values from 1 to 9 and prints the array to the console. Expected output: 1 2 3 4 5 6 7 8 9
//int[,] a = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
//for (int i = 0; i < 3; i++)
//{
//    for (int j = 0; j < 3; j++)
//    {
//        Console.Write(a[i, j] + " ");
//    }
//}
//foreach (var item in a)
//{
//    Console.Write(item+" ");
//}
//2.Write a program that initializes a 2D array of integers with random values between 1 and 100 and prints the array to the console. Expected output: [random array of integers between 1 and 100]
//int rows = Random.Shared.Next(1, 4);
//int columns = Random.Shared.Next(1, 4);
//int[,] a = new int[3, 3];
//for (int i = 0; i < 3; i++)
//{
//    for (int j = 0; j < 3; j++)
//    {
//        a[i, j] = Random.Shared.Next(1, 101);
//        Console.Write(a[i, j] + " ");
//    }
//    Console.WriteLine();
//}
//foreach (int i in a)
//{
//    Console.Write(i + " ");
//}

//3.Write a program that initializes a 2D array of characters with the letters of the alphabet in order and prints the array to the console. Expected output: a b c d e f g h i j k l m n o p q r s t u v w x y z
//char[,] alphabet = new char[1, 26];
//int i;
//while (i < 26)
//{
//    alphabet[0, i] = (char)('a' + i);
//    i++;
//}

//for (i = 0; i < 26; i++)
//{
//    Console.Write(alphabet[0, i] + " ");
//}

//4. Write a program that initializes a 2D array of integers with values from 1 to 25 and prints only the even numbers to the console. Expected output: 2 4 6 8 10 12 14 16 18 20 22 24
//var arr = new int[5, 5];
//int count = 1;
//for (int i = 0; i < 5; i++)
//{
//    for (int j = 0; j < 5; j++)
//    {
//        arr[i, j] = count;
//        count++;
//        if (arr[i, j] % 2 == 0)
//        {
//            Console.WriteLine(arr[i, j] + " ");
//        }
//    }
//}
//5.Write a program that initializes a 2D array of strings with names of countries and their capitals, then prints each country and its capital to the console. Expected output: USA Washington D.C. France Paris Japan Tokyo Mexico Mexico City
//string[,] countries = { {"USA", "Washington D.C."},
//                                {"France", "Paris"},
//                                {"Japan", "Tokyo"},
//                                {"Mexico", "Mexico City"} };
//for (int i = 0; i < countries.GetLength(0); i++)
//{
//    string country = countries[i, 0];
//    string capital = countries[i, 1];
//    Console.WriteLine(country + " " + capital);
//}
//6. Write a program that initializes a 2D array of integers with values from 1 to 16 and prints the diagonal elements of the array to the console. Expected output: 1 6 11 16
//int count = 1;
//var arr = new int[4, 4];
//for (int i = 0; i < 4; i++)
//{
//    for (int j = 0; j < 4; j++)
//    {
//        arr[i, j] = count;
//        count++;
//    }
//}
//for (int i = 0; i < 4; i++)
//{
//    Console.WriteLine(arr[i, i] + " ");
//}

//7.Write a program that initializes a 2D array of integers with random values between 1 and 100, then finds and prints the smallest value in the array. Expected output: The smallest value in the array is: [smallest value]
//var arr = new int[5, 5];
//for (int i = 0; i < 5; i++)
//{
//    for (int j = 0; j < 5; j++)
//    {
//        arr[i, j] = Random.Shared.Next(1, 101);
//        Console.Write(arr[i, j] + " ");
//    }
//    Console.WriteLine();
//}
//    int indexrow = 0, indexcol = 0;
//    int min = arr[0, 0];
//    for (int i = 0; i < 5; i++)
//    {
//        for (int j = 0; j < 5; j++)
//        {
//            if (arr[i, j] < min)
//            {
//                min = arr[i, j];
//                indexrow = i;
//                indexcol = j;
//            }
//        }
//    }
//Console.WriteLine($"Minimum value in the array:{min} " +  ", at index [" + indexrow + ", " + indexcol + "]");

//8.Write a program that initializes a 2D array of integers with random values between 1 and 100, then finds and prints the sum of all the values in the array. Expected output: The sum of all the values in the array is: [sum]
//var arr = new int[10, 10];
//int sum = 0;
//for (int i = 0; i < 10; i++)
//{
//    for (int j = 0; j < 10; j++)
//    {
//        arr[i, j] = Random.Shared.Next(1, 101);
//        sum+= arr[i, j];
//    }
//}
//Console.WriteLine(sum);

//9.Write a program that initializes a 2D array of integers with values from 1 to 100, then finds and prints the average of the values in the array. Expected output: The average of all the values in the array is: [average]
//var arr = new int[10, 10];
//int sum = 0;
//int count = 0;
//for (int i = 0; i < 10; i++)
//{
//    for (int j = 0; j < 10; j++)
//    {
//        arr[i, j] = count;
//        count++;
//        //sum += arr[i, j];
//    }
//}
//double Avg(int[,] a)
//{
//    foreach (int i in arr)
//    {
//        sum += i;
//        count++;
//    }
//    return (double)sum / 100;
//}
//Console.WriteLine(Avg(arr));

//10.Write a program that initializes a 2D array of integers with random values between 1 and 100, then sorts each row of the array in ascending order and prints the sorted array to the console. Expected output: [sorted array of integers]
//initialize the 2d array with random values
var array = new int[5, 5];
for (int i = 0; i < 5; i++)
{
    for (int j = 0; j < 5; j++)
    {
        array[i, j] = Random.Shared.Next(1, 101);
    }
}

//column
//for (int j = 0; j < array.GetLength(1); j++)
//{
//    for (int i = 0; i < array.GetLength(0) - 1; i++)
//    {
//        int min_index = i;
//        for (int k = i + 1; k < array.GetLength(0); k++)
//        {
//            if (array[k, j] < array[min_index, j])
//            {
//                min_index = k;
//            }
//        }
//        if (min_index != i)
//        {
//            int temp = array[i, j];
//            array[i, j] = array[min_index, j];
//            array[min_index, j] = temp;
//        }
//    }
//}
//row
//for (int i = 0; i < array.GetLength(0); i++)
//{
//    // Sort each row of the array in ascending order.  
//    for (int j = 0; j < array.GetLength(1) - 1; j++)
//        for (int k = 0; k < 5 - j - 1; k++)
//        {
//            if (array[i, k] > array[i, k + 1])
//            {
//                int temp = array[i, k];

//                array[i, k] = array[i, k + 1];

//                array[i, k + 1] = temp;
//            }
//        }
//}
// Print the sorted array
//for (int i = 0; i < 5; i++)
//{
//    for (int j = 0; j < 5; j++)
//    {
//        Console.Write("{0,3}", array[i, j]);
//    }
//    Console.WriteLine();
//}


//11.Write a program that initializes a 2D array of integers with random values between 1 and 100, then finds and prints the largest value in the array. Expected output: The largest value in the array is: [largest value]
//var a = new int[10, 10];
//int index0 = 0, index1 = 0;
//var max = a[0, 0];
//for (int i = 0; i < 10; i++)
//{
//    for (int j = 0; j < 10; j++)
//    {
//        a[i, j] = Random.Shared.Next(1, 101);
//        Console.Write(a[i, j] + " ");
//    }
//    Console.WriteLine();
//}
//for (int i = 0; i < 10; i++)
//{
//    for (int j = 0; j < 10; j++)
//    {
//        if (max < a[i, j])
//        {
//            max = a[i, j];
//            index0 = i;
//            index1 = j;
//        }
//    }
//}
//Console.WriteLine($"Max is {max} in index [{index0},{index1}]");
//12.Write a program that initializes a 2D array of integers with random values between 1 and 100, then finds and prints the median value in the array. Expected output: The median value in the array is: [median value]
//var a = new int[10, 10];
//for (int i = 0; i < 10; i++)
//{
//    for (int j = 0; j < 10; j++)
//    {
//        a[i, j] = Random.Shared.Next(1, 101);
//        Console.Write(a[i, j] + " ");
//    }
//    Console.WriteLine();
//}
//var b=new int[100];
//int count = 0;
//for (int i = 0;i < 10; i++)
//{
//    for (int j = 0; j < 10; j++)
//    {
//        b[count++] = a[i,j];
//    }
//}
//Array.Sort(b);
//int middle = b.Length / 2;
//double median;
//if (b.Length % 2 == 0)
//{
//    median = (b[middle] + b[middle - 1]) / 2.0;
//}
//else
//{
//    median = b[middle];
//}
//Console.WriteLine("The median value in the array is: " + median);
//13.Write a program that initializes a 2D array of integers with random values between 1 and 100, then prints the row and column indices of the minimum value in the array to the console. Expected output: The minimum value in the array is located at row [row] and column[column].
//14. Write a program that initializes a 2D array of integers with random values between 1 and 100, then prints the row and column indices of the maximum value in the array to the console. Expected output: The maximum value in the array is located at row [row]
//var a = new int[10, 10];
//for (int i = 0; i < 10; i++)
//{
//    for (int j = 0; j < 10; j++)
//    {
//        a[i, j] = Random.Shared.Next(1, 101);
//        Console.Write(a[i, j] + " ");
//    }
//    Console.WriteLine();
//}
//int index0 = 0, index1 = 0, index2 = 0, index3 = 0;
//var min = a[0, 0];
//var max = a[0, 0];

//for (int i = 0; i < 10; i++)
//{
//    for (int j = 0; j < 10; j++)
//    {
//        if (a[i, j] < min)
//        {
//            min = a[i, j];
//            index0 = i;
//            index1 = j;
//        }
//        if (a[i, j] > max)
//        {
//            max = a[i, j];
//            index2 = i;
//            index3 = j;
//        }
//    }
//}

//Console.WriteLine($"Max is {max} in index [{index2},{index3}]");
//Console.WriteLine($"Min is {min} in index [{index0},{index1}]");


