﻿
using System.ComponentModel.DataAnnotations;
using System.Reflection.Metadata.Ecma335;

const int maxQueue = 4;
var a = new int[maxQueue];
int head = -1;
int count = 0;

void Enqueue(int number)
{
    //CHECK QUEUE IS FULL
    if (count == maxQueue)
    {
        throw new InvalidOperationException();
    }
    else if (count == 0)//CHECK QUEUE IS EMPTY
    {
        a[0] = number;//number is in index 0
        head = 0;//head pont to index 0
        count = 1;//increase count
        return;//exit
    }
    else if (head + count < maxQueue)//CHECK SPACE FOR NUMBER
    {
        a[head + count] = number;//add new number for index next 
    }
    else
    {
        a[maxQueue - (head + count)] = number;//CHECK IS FULL RETURN TO THE INDEX 0
    }
    count++; //increase the item of array
}

int Dequeue()
{
    if (count == 0)
    {
        throw new InvalidOperationException();
    }

    int number = a[head] = 0;

    if (head == maxQueue - 1)
    {
        if (count == 1)
        {
            head = -1;
        }
        else
        {
            head = 0;
        }
    }
    else
    {
        head++;
    }
    count--;
    return number;
}

int Peek()
{
    if (head == -1 || count == 0)
    {
        throw new InvalidOperationException();
    }

    return a[head];
}
Enqueue(1);
Enqueue(2);
Enqueue(3);
Enqueue(4);
Dequeue();
Dequeue();

foreach (var item in a)
{
    if (item == 0)
    {
        continue;
    }
    Console.Write(item + " ");
}
//using System.Collections;

//Queue q=new Queue();
//q.Enqueue(1);
//q.Enqueue(2);
//q.Enqueue(3);
//q.Enqueue(4);

//q.Dequeue();
//foreach(var item in q)
//{
//    Console.Write(item.ToString()+" ");
//}







