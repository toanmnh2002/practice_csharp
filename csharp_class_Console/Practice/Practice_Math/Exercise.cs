﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practice_Math
{

    public class Exercise
    {
        static void Main(string[] args)
        {
            //Abs_Ex1();
            //Abs_Ex2();
            //Abs_Ex3();
            //Abs_Ex5();
            //Tuple_Ex6();
            //Ex7();
            //Ex12();
            //Ex18();
            //Ex19();
            //Ex21();
            //Ex22();
            Ex23();

        }
        private static void Abs_Ex1()
        {
            decimal deci;
            bool dec;
            do
            {
                Console.Write("Enter decimal number: ");
                dec = decimal.TryParse(Console.ReadLine(), out deci);
            }
            while (!dec);
            Console.WriteLine("Abs for decimal number: " + Math.Abs(deci));

            double dou;
            bool doul;
            do
            {
                Console.Write("Enter double number: ");
                doul = double.TryParse(Console.ReadLine(), out dou);
            }
            while (!doul);
            Console.WriteLine("Abs for double number: " + Math.Abs(dou));
            Int16 a;
            bool chec;
            do
            {
                Console.Write("Enter int16 num: ");
                chec = Int16.TryParse(Console.ReadLine(), out a);
            } while (!chec);
            Console.WriteLine("Abs for int16 num: " + Math.Abs(a));
            Int32 b;
            bool che;
            do
            {
                Console.Write("Enter int32 num: ");
                che = Int32.TryParse(Console.ReadLine(), out b);
            } while (!che);
            Console.WriteLine("Abs for int16 num: " + Math.Abs(b));




            long[] values3 = { Int64.MaxValue, 109013, 0, -6871982, Int64.MinValue };
            Console.WriteLine("Absolute value of a number of Int64 values:");
            foreach (long value in values3)
            {
                try
                {
                    Console.WriteLine($"Abs({value}) = {Math.Abs(value)}");

                }
                catch (OverflowException)
                {
                    Console.WriteLine("Unable to calculate the absolute value of {0}.",
                      value);
                }
            }





        }
        private static void Abs_Ex2()
        {
            string statement = "The greater of {1} and {2} is {3}";
            byte xByte1 = 1, xByte2 = 51;
            Console.WriteLine(statement, "Byte   ", xByte1, xByte2, Math.Max(xByte1, xByte2));
        }
        private static void Abs_Ex3()
        {
            for (int i = 0; i <= 32; i++)
            {
                Console.WriteLine("3^{0}={1}", i, Math.Pow(3, i));
            }
        }
        private static void Abs_Ex5()
        {
            string str = "{0}: {1} is {2} zero.";
            byte xByte1 = 0;
            Console.WriteLine(str, "Byte   ", xByte1, Test(Math.Sign(xByte1)));

        }
        private static string Test(int i)
        {
            if (i == 0) return $"equal to 0";
            return null;
        }
        private static void Tuple_Ex6()
        {
            Tuple<string, double>[] team =
                {

                Tuple.Create("Felix",11.3),
                Tuple.Create("Mudrk",12.3),
                Tuple.Create("Enzo",13.5),
                Tuple.Create("Badishille",14.5)
        };
            Console.Write("{0,5} {1,10} \n", "Name", "Number");
            foreach (var t in team)
                Console.WriteLine("{0,5} {1,10}",
                  t.Item1, t.Item2);
        }
        private static void Ex7()
        {
            Console.WriteLine(Math.Truncate(534.334));
        }
        private static void Ex12()
        {
            var n = 23132;
            Console.WriteLine("Original number: " + n);
            Console.WriteLine("test palindrome number: " + Palindrome(n));
        }
        private static bool Palindrome(int a)
        {
            if (a < 0) { return false; }
            if (a < 10)
            {
                return true;
            }
            var temp = a;
            var b = 0;
            var digit = 0;
            while (temp != 0)
            {
                digit = temp % 10;
                b = b * 10 + digit;
                temp /= 10;
            }
            return a == b;
        }
        private static void Ex18()
        {
            int[] num = { -10, -11, -12, -13, -14, 15, 16, 17, 18, 19, 20 };
            Console.WriteLine("Array show: ");
            foreach (var t in num) Console.Write(t + "\t");
            Console.WriteLine("\nArray sum: " + CalculateSum(num));
        }
        private static int CalculateSum(int[] num)
        {
            int[] sum = { num.Where(x => x > 0).Sum(), num.Where(x => x > 0).Sum() };
            return sum.OrderByDescending(x => Math.Abs(x)).First();
        }
        private static void Ex19()
        {
            int n;
            bool check;
            do
            {
                Console.Write("Enter n: ");
                check = int.TryParse(Console.ReadLine(), out n);
            } while (!check);
            Console.Write("Value approximately pi up to\t" + n + "\tis\t" + test(n));
        }
        private static decimal test(int n)
        {
            return decimal.Round(3.12323232m, n);
        }
        private static void Ex21()
        {
            int n;
            bool check;
            do
            {
                Console.Write("Enter n: ");
                check = int.TryParse(Console.ReadLine(), out n);
            } while (!check);
            Console.Write("Calculate\t" + Calculate(n));
        }
        public static int Calculate(int n)
        {
            return n * (n + 1) * (n + 2) / 6;
        }
        private static void Ex22()
        {
            int n;
            bool check;
            do
            {
                Console.Write("Enter n: ");
                check = int.TryParse(Console.ReadLine(), out n);
            } while (!check);
            Console.WriteLine("Descending order: " + desc(n));
            Console.WriteLine("Aescending order: " + asc(n));

        }
        private static int desc(int num)
        {
            return int.Parse(string.Concat(num.ToString().OrderByDescending(x => x)));
        }
        private static int asc(int num)
        {
            return int.Parse(string.Concat(num.ToString().OrderBy(x => x)));
        }
        private static void Ex23()
        {
            int n;
            bool check;
            do
            {
                Console.Write("Enter n: ");
                check = int.TryParse(Console.ReadLine(), out n);
            } while (!check);
            Console.WriteLine(n + "\tis" + chec(n));
        }
        private static string chec(int n)
        {
            string str = n.ToString();
            int total = 0;
            for (int i = 0; i < str.Length; i++)
                total += int.Parse(str[i].ToString());
            return (total % 2 == 0 ? "Evenish" : "Oddish");
        }
    }
}



