using FluentAssertions;

namespace Test.tests
{
    public class UnitTest1
    {
        //1
        [Fact]
        public static void ReturnLength_ShouldRetrunCorrectValue_WhenTheStringIsNotNull()
        {
            //arrange
            var expected = 5;
            var str = "hello";
            //act
            var actual = Program.ReturnString(str);
            //assert
            actual.Should().Be(expected);
        }

        //2
        [Fact]
        public static void SumTwoInteger_ShouldRetrunCorrectValue_WhenBothNumberArePossitive()
        {
            //arrange
            var expected = 6;
            var a = 5;
            var b = 1;
            //act
            var actual = Program.SumTwoInteger(a, b);
            //assert
            actual.Should().Be(expected);
        }

        [Fact]
        public static void SumTwoInteger_ShouldRetrunCorrectValue_WhenBothNumberAreNegative()
        {
            //arrange
            var expected = -15;
            var a = -5;
            var b = -10;
            //act
            var actual = Program.SumTwoInteger(a, b);
            //assert
            actual.Should().Be(expected);
        }

        [Fact]
        public static void SumTwoInteger_ShouldReturnZero_WhenBothNumberAreZero()
        {
            //arrange
            var expected = 0;
            var a = 0;
            var b = 0;
            //act
            var actual = Program.SumTwoInteger(a, b);
            //assert
            actual.Should().Be(expected);
        }
        //3
        [Fact]
        public static void SumArray_ShouldReturnCorrectValue_WhenArrayIsNotEmpty()
        {
            //arrange
            var a = new int[] { 1, 2, 3, 4, 5, 6 };
            var expected = 21;
            //act
            int actual = Program.SumArray(a);
            //assert
            actual.Should().Be(expected);
        }
        //4
        [Fact]
        public static void CountChar_ShouldReturnCorrectValue_WhenListIsNotEmpty()
        {
            //arrange
            var input = "hello";
            var character = 'l';
            var expected = 2;
            //act
            int actual = Program.CountChar(input, character);
            //assert
            actual.Should().Be(expected);
        }
        //5
        [Fact]
        public static void ReturnLargestIntegerInList_ShouldReturnCorrectValue_WhenListIsNotEmpty()
        {
            //arrange
            var list = new List<int>() { 5, 3, 7, 1, 9, 22, 55 };
            var expected = 55;
            //act
            int actual = Program.ReturnLargestIntegerInList(list);
            //assert
            actual.Should().Be(expected);
        }

        //6
        [Fact]
        public static void RemoveVowels_ShouldReturnCorrectString()
        {
            //arrange
            string input = "hello";
            var expected = "hll";
            //act
            var actual = Program.RemoveVowels(input);
            //assert
            actual.Should().Be(expected);
        }
        //7
        [Fact]
        public static void ReturnString_ShouldReturnCorrectValue()
        {
            //arrange
            var a = new List<string>() { "tester", "testted", "testest", "hello", "heols" };
            var expected = 3;
            //act
            var actual = Program.ReturnString(a);
            //assert
            actual.Should().Be(expected);
        }
        //8
        [Fact]
        public static void GetEvenNumberFromList_ShouldReturnCorrectValue()
        {
            //arrange
            var a = new List<int>() { 1, 2, 3, 5, 9 };
            var expected = new List<int>() { 1, 5, 3, 9 };
            //act
            var actual = Program.GetEvenNumberFromList(a);
            //assert
            actual.Should().BeEquivalentTo(expected);
        }

        //9
        [Fact]
        public static void GetStringStartFromList_ShouldReturnCorrectValue()
        {
            //arrange
            var a = new List<string>() { "felix","mount","havertz","madueke","enzo","adams" };
            var expected = new List<string>() { "enzo", "adams" };
            //act
            var actual = Program.GetStringStartFromList(a);
            //assert
            actual.Should().BeEquivalentTo(expected);
        }

        [Fact]
        public static void ReverseString_ShouldReturnCorrectValue()
        {
            //arrange
            string input = "hello world";
            string expectedOutput = "dlrow olleh";
            //act
            var actual = Program.ReverseString(input);
            //assert
            actual.Should().BeEquivalentTo(expectedOutput);
        }
    }
}
