﻿public class Program
{
    public static void Main()
    {
        GetDayOfWeek(5);
    }
    public static string GetDayOfWeek(int dayNumber)
    {
        return dayNumber switch
        {
            1 => "Monday",
            2 => "Tuesday",
            3 => "Wednesday",
            4 => "Thursday",
            5 => "Friday",
            6 => "Saturday",
            7 => "Sunday",
            _ => "Invalid day number",
        };
    }

}