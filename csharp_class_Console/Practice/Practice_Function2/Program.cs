﻿//Write a program that prints out the first 10 positive integers.
//void Print10()
//{
//    void PrintInteger(int n)
//    {
//        Console.WriteLine(n + " ");
//    }
//    for (int i = 1; i <= 10; i++)
//    {
//        PrintInteger(i);
//    }
//}
//Print10();
//Write a program that prints out the first 10 even positive integers.

//void Print10Even()
//{
//    void PrintInteger(int n)
//    {
//        if (n % 2 != 0)
//        {
//            Console.WriteLine(n + " ");
//        }
//    }
//    for (int i = 1; i <= 10; i++)
//    {
//        PrintInteger(i);
//    }
//}
//Print10Even();

//Write a program that prints out the first 10 odd positive integers.
//void Print10Even()
//{
//    void PrintInteger(int n)
//    {
//        if (n % 2 != 0)
//        {
//            Console.WriteLine(n + " ");
//        }
//    }
//    for (int i = 1; i <= 10; i++)
//    {
//        PrintInteger(i);
//    }
//}
//Print10Even();
//Write a program that prints out the first 10 numbers in the Fibonacci sequence.
//void Print10Odd()
//{
//    void PrintInteger(int n)
//    {
//        if (n%2 == 0)
//        {
//            Console.WriteLine(n + " ");
//        }
//    }
//    for (int i = 1; i <= 10; i++)
//    {
//        PrintInteger(i);
//    }
//}
//Print10Odd();
//Write a program that prints out the first 10 prime numbers.
//int Fibo(int n)
//{
//    if (n < 2) return 1;
//	else
//	{
//		return Fibo(n - 1)+Fibo(n-2);
//	}
//}
//Console.Write("Enter num: ");
//int num = int.Parse(Console.ReadLine());
//Console.WriteLine("0");
//for (int i = 0; i < num-1; i++)
//{
//	Console.WriteLine(Fibo(i)+" ");
//}

//int Fibo(int nno)
//{
//    int num1 = 0; // Initialize num1 to 0
//    int num2 = 1; // Initialize num2 to 1
//    int i = 0; // Initialize i to 0

//    while (i < nno) // While i is less than nno
//    {
//        int temp = num1; // Assign the value of num1 to temp
//        num1 = num2; // Assign the value of num2 to num1
//        num2 = temp + num2; // Assign the sum of temp and num2 to num2
//        i++; // Increment i by 1
//    }

//    return num1; // Return the value of num1
//}
//Console.Write("Enter n: ");
//int n = int.Parse(Console.ReadLine());
//for (int i = 0; i < n; i++)
//{
//    Console.Write(Fibo(i) + "  ");
//}


//Write a program that prints out the multiplication table for numbers 1 through 10.
//bảng cửu chương
//void Multiplication()
//{
//    for (int i = 1; i <= 10; i++) // outer loop for first factor
//    {
//        for (int j = 1; j <= 10; j++) // inner loop for second factor
//        {
//            Console.Write("{0}\t", i * j); // multiply i and j and print the result with a tab character
//        }
//        Console.WriteLine();
//    }
//}
//Multiplication();
// move to the next line after printing all numbers in a row
//Write a program that prints out the first 10 numbers in the sequence 1, 4, 7, 10, 13, 16, 19, 22, 25, 28.
//void Sum3sequence()
//{
//    int sum = 0;
//    for (int i = 1; i <= 28; i += 3)
//    {
//        Console.WriteLine(i);
//        sum = sum + i;
//    }
//}

//Write a program that prompts the user to enter a number and then prints out all the positive integers less than or equal to that number.
//var random = Random.Shared.Next(1, 101);
//Console.WriteLine($"Number is {random}");
//void Count()
//{
//	for (int i = 0; i <= random; i++)
//	{
//		Console.Write(i+" ");
//	}
//}
//Count();
//Write a program that prompts the user to enter a number and then prints out all the prime numbers less than or equal to that number.
//Console.Write("Enter number: ");
//int n = int.Parse(Console.ReadLine());
//void Prime()
//{
//    for (int i = 2; i <= n; i++) // loop over all numbers from 2 to n
//    {
//        bool isPrime = true;

//        for (int j = 2; j <= Math.Sqrt(i); j++) // loop over all numbers from 2 to the square root of i
//        {
//            if (i % j == 0) // check if i is divisible by j
//            {
//                isPrime = false;
//                break; // if i is divisible by j, break out of the inner loop
//            }
//        }
//        if (isPrime) // if isPrime is still true after checking all possible divisors, i is prime
//        {
//            Console.Write("{0} ", i); // print the current prime number
//        }
//    }
//}
//Prime();
//Write a program that prompts the user to enter a number and then prints out the sum of all the positive integers less than or equal to that number.
//Console.Write("Enter number: ");
//int n = int.Parse(Console.ReadLine());
//int Sum()
//{
//    int sum = 0;
//    for (int i = 1; i <= n; i++)
//    {
//        sum += i;
//    }
//    return sum;
//}
//Console.WriteLine(Sum());


//Write a method called sum that takes two integer parameters and returns the sum of the two integers.

//void Sum1()
//{
//    var a = Random.Shared.Next(1, 101);
//    var b = Random.Shared.Next(1, 101);
//    Console.WriteLine($"a+b={a}+{b}={a + b}");
//}
//Sum1();
//Write another method called sum that takes three integer parameters and returns the sum of the three integers.
//int Sum2(int a, int b, int c)
//{
//    return a + b + c;
//}
//Console.WriteLine(Sum2(50, 10, 100));

//Write a method called sum that takes an array of integers and returns the sum of the integers in the array.

//int[] a = { 1, 2, 3, 4, 5, 6 };
//int Sum3(int[] arr)
//{
//    int sum = 0;
//    for (int i = 0; i < a.Length; i++)
//    {
//        sum += a[i];
//    }
//    return sum;
//}
//Console.WriteLine($"Sum is {Sum3(a)}");
//Write a method called average that takes an array of integers and returns the average of the integers in the array.

//int[] a = { 1, 2, 3, 4, 5, 6 };
//double Average(int[] a)
//{
//    int sum = 0;
//    for (int i = 0; i < a.Length; i++)
//    {
//        sum += a[i];
//    }
//    return (double)sum / a.Length;
//}
//Console.WriteLine($"Average is {Average(a)}");

//Write a method called min that takes an array of integers and returns the smallest integer in the array.
//int[] arr = { 1, 2, 3, 4, 5, 6 };
//int Min(int[] a)
//{
//    for (int i = 0; i < arr.Length; i++)
//    {
//        if (arr[i] < arr[0])
//        {
//            arr[0] = arr[i];
//        }
//    }
//    return a[0];
//}
//Console.WriteLine($"Min is {Min(a)}");
//Console.WriteLine($"Min is {a.Min()}");

//Write a method called max that takes an array of integers and returns the largest integer in the array.


//int[] a = { 1, 2, 3, 4, 5, 6 };
//int Max(int[] a)
//{
//	for (int i = 0; i < a.Length; i++)
//	{
//		if (a[i] > a[0])
//		{
//			a[0] = a[i];
//		}
//	}
//	return a[0];
//}
//Console.WriteLine($"Max is {Max(a)}");
//int[] numbers = { 10, 2, 4, 6, 8 };
//int largest = numbers.Max();
//Console.WriteLine("The largest number in the array is {0}.", largest);
//Write a method called printArray that takes an array of integers and prints each integer on a new line.
//int[] a = { 1, 2, 3, 4, 5, 6 };
//void PrintArray()
//{
//	foreach (var item in a)
//	{
//		Console.Write(item+ " ");
//	}
//}
//PrintArray();

//Write a main method that creates an array of integers, populates it with values, and calls each of the above methods at least once. Test that each method works as expected with appropriate inputs.
//int k;
//bool check;
//do
//{
//    Console.Write("Enter size of array: ");
//    check = int.TryParse(Console.ReadLine(), out k);
//} while (!check);

//int[] array = new int[k];

//for (int i = 0; i < k; i++)
//{
//    bool inputCheck;
//    do
//    {
//        Console.Write($"array[{i}]: ");
//        inputCheck = int.TryParse(Console.ReadLine(), out array[i]);
//        if (!inputCheck)
//        {
//            Console.WriteLine("Invalid input. Please enter a valid integer.");
//        }
//    } while (!inputCheck);
//}

//Console.WriteLine("The array is: " + string.Join(", ", array));



//int Sum(int n)
//{
//    if (n == 1)
//    {
//        return 1;
//    }
//    else
//    {
//        return n + Sum(n - 1);
//    }
//}
//Console.WriteLine(Sum(5));
//int NFactorial(int n)
//{
//    if (n == 0)
//    {
//        return 1;
//    }
//    else
//    {
//        return n * NFactorial(n - 1);
//    }
//}




