﻿

//1.Write a program in C# Sharp to create a user define function. Go to the editor
//Expected Output :
//Welcome Friends!
//Have a nice day!
//Click me to see the solution
//void Hello() => Console.WriteLine("Welcome Friends!");
//void Have() => Console.WriteLine("Have a nice day!");
//Hello();
//Have();



//2. Write a program in C# Sharp to create a user define function with parameters. Go to the editor
//Test Data :
//Please input a name : John
//Expected Output :
//Welcome friend John !
//Have a nice day!
//Click me to see the solution

//void Hello(string name)=> Console.WriteLine($"Welcome friend {name} ");
//Console.Write("Enter a name: ");
//string str1 = Console.ReadLine(); 
//Hello(str1);
//3. Write a program in C# Sharp to create a function for the sum of two numbers. Go to the editor
//Test Data :
//Enter a number: 15
//Enter another number: 16
//Expected Output :
//The sum of two numbers is : 31
//Click me to see the solution

//var a = Random.Shared.Next(1, 101);
//var b = Random.Shared.Next(1, 101);
//Console.WriteLine(a+b);

//4. Write a program in C# Sharp to create a function to input a string and count number of spaces are in the string. Go to the editor
//Test Data :
//Please input a string : This is a test string.
//Expected Output :
//"This is a test string." contains 4 spaces
//Click me to see the solution

//int CountSpace(string str)
//{
//    int space = 0;
//    string str1;//store ""
//    for (int i = 0; i < str.Length; i++)
//    {
//        str1=str.Substring(i,1);
//        if (str1 == " ") space++;
//    }
//    return space;
//}
//Console.WriteLine("Enter string: ");
//string str2 = Console.ReadLine();
//Console.WriteLine($"{str2} has {CountSpace(str2)} spaces");

//5. Write a program in C# Sharp to calculate the sum of elements in an array. Go to the editor
//Test Data :
//Input 5 elements in the array :
//element - 0 : 5
//element - 1 : 7
//element - 2 : 3
//element - 3 : 2
//element - 4 : 9
//Expected Output :
//The sum of the elements of the array is 26
//Click me to see the solution

//int SumArray(int[] arr)
//{
//    int sum = 0;
//	for (int i = 0; i < arr.Length; i++)
//	{
//		sum += arr[i];
//	}
//	return sum;
//}
//int[] arr=new int[5];
//for (int i = 0; i < arr.Length; i++)
//{
//	Console.Write($"element {i}: ");
//	arr[i] = Convert.ToInt32(Console.ReadLine());
//}
//Console.WriteLine(SumArray(arr)); 
//6. Write a program in C# Sharp to create a function to swap the values of two integer numbers. Go to the editor
//Test Data :
//Enter a number: 5
//Enter another number: 6
//Expected Output :
//Now the 1st number is : 6 , and the 2nd number is : 5
//Click me to see the solution
//void swap(ref int a, ref int b)
//{
//    int temp;
//    temp = a;
//    a = b; b = temp;
//}
//Console.Write("Enter a: ");
//int a = int.Parse(Console.ReadLine());
//Console.Write("Enter b: ");
//int b = int.Parse(Console.ReadLine());
//swap(ref a,ref b);
//Console.WriteLine($"a is {a}\nb is {b}");
//7. Write a program in C# Sharp to create a function to calculate the result of raising an integer number to another. Go to the editor
//Test Data :
//Input Base number: 3
//Input the Exponent : 2
//Expected Output :
//So, the number 3 ^ (to the power) 2 = 9
//Click me to see the solution

//double luythua(int n,int b)
//{
//    return Math.Pow(n, b);
//}
//Console.WriteLine("Enter n: ");
//int n=int.Parse(Console.ReadLine());
//Console.WriteLine("Enter b: ");
//int b = int.Parse(Console.ReadLine());
//Console.WriteLine(luythua(n,b));
//8. Write a program in C# Sharp to create a function to display the n number Fibonacci sequence. Go to the editor
//Test Data :
//Input number of Fibonacci Series : 5
//Expected Output :
//The Fibonacci series of 5 numbers is :
//0 1 1 2 3
//Click me to see the solution
// int Fibo(int nno)
//{
//    int num1 = 0; // Initialize num1 to 0
//    int num2 = 1; // Initialize num2 to 1
//    int i = 0; // Initialize i to 0

//    while (i < nno) // While i is less than nno
//    {
//        int temp = num1; // Assign the value of num1 to temp
//        num1 = num2; // Assign the value of num2 to num1
//        num2 = temp + num2; // Assign the sum of temp and num2 to num2
//        i++; // Increment i by 1
//    }

//    return num1; // Return the value of num1
//}
//Console.Write("Enter n: ");
//int n=int.Parse(Console.ReadLine());
//for (int i = 0; i < n; i++)
//{
//    Console.Write(Fibo(i) + "  ");
//}



//9. Write a program in C# Sharp to create a function to check whether a number is prime or not. Go to the editor
//Test Data :
//Input a number : 31
//Expected Output :
//31 is a prime number
//Click me to see the solution

//bool CheckPrime(int num)
//{
//	for (int i = 2; i < num; i++)
//	{
//		if(num%i==0) return false;
//	}
//    return true;
//}
//Console.Write("Input a number : ");
//int n = Convert.ToInt32(Console.ReadLine());
//if (CheckPrime(n)){
//	Console.WriteLine("is prime");
//}
//else Console.WriteLine("not");


//10. Write a program in C# Sharp to create a function to calculate the sum of the individual digits of a given number. Go to the editor
//Test Data :
//Enter a number: 1234
//Expected Output :
//The sum of the digits of the number 1234 is : 10
//Click me to see the solution


//int Sum(int n)
//{
//string str=Convert.ToString(n);
//    int sum = 0;
//	for (int i = 0; i<str.Length ; i++)
//	{
//		sum += Convert.ToInt32(str.Substring(i,1));
//	}
//	return sum;
//}
//Console.Write("Enter a number: ");
//int num = Convert.ToInt32(Console.ReadLine());
//Console.WriteLine("The sum of the digits of the number {0} is : {1} \n", num, Sum(num));

//11. Write a program in C# Sharp to create a recursive function to find the factorial of a given number. Go to the editor
//Test Data :
//Enter a number: 5
//Expected Output :
//The factorial of 5! is 120
//Click me to see the solution

//Console.Write("Input a number : ");
//int num = Convert.ToInt32(Console.ReadLine());
//decimal f = Factorial(num);
//Console.WriteLine($"{num}!={f}");
//static decimal Factorial(int n1)
//{
//    if (n1 == 0)
//    {
//        return 1;
//    }
//    else
//    {
//        return n1 * Factorial(n1 - 1);
//    }
//}



//12. Write a program in C# Sharp to create a recursive function to calculate the Fibonacci number of a specific term. Go to the editor
//Test Data :
//Enter a number: 10
//Expected Output :
//The Fibonacci of 10 th term is 55
//Click me to see the solution
int fibonacci(int n)
{
    if (n < 2)
    {
        return 1;
    }
    else
    {
        return fibonacci(n - 1)+fibonacci(n-2);
    }

}
Console.Write("Enter a number: ");
int num = Convert.ToInt32(Console.ReadLine());
Console.WriteLine("0"+" ");
for(int i = 0; i < num-1; i++)
{
    Console.WriteLine(fibonacci(i));
}