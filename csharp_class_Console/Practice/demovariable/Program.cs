﻿// See https://aka.ms/new-console-template for more information
using System.ComponentModel.DataAnnotations;
using System;
using System.Numerics;
//DataType variableName;
////System.SByte test;
//System.SByte test = 127;

//System.Byte test2 = 127;
//sbyte decimalLiteral = 42;
//sbyte hexLiteral = 0x2A;
//byte binaryLiteral = 0b_0010_1010;
//byte number = 0b_1011011;
//Console.WriteLine(test);
//Console.WriteLine(test2);
//Console.WriteLine(decimalLiteral);
//Console.WriteLine(hexLiteral);
//Console.WriteLine(binaryLiteral);
//Console.WriteLine(number);
////size of oprator 
//Console.WriteLine("Size of sbyte is " +sizeof(sbyte));
//Console.WriteLine("Size of boolean is "+sizeof(bool));
//Console.WriteLine("Size of integer is "+sizeof(int));  
//Console.WriteLine("Size of decimal is "+sizeof(decimal));
//Console.WriteLine("Size of short is "+sizeof(short));

//int x=10;
//int x1=x;
//unsafe{
//    console.WriteLine("The memory address of x is: "+(int&x));
//        console.WriteLine("The memory address of x is: "+(int&x1));

//}
//int x=1;
//int y=x;
//bool valueResult=ReferenceEquals(x,y);
//Console.WriteLine(valueResult);

////fault
//static void hello()
//{
//    hello();
//}
//hello();


//stack->value
//heap->reference 

//var c = 120.23f;
//var x = "hello";
//Console.WriteLine("value of c {0},type {1}",c,c.GetType());
//Console.WriteLine("value of x {0},type {1}", x, x.GetType());

//implicity conversion(s->b)
//sbyte small = 100;
//byte bytes = small;

//exlicity conversion(b->s)
//double x = 1234.7;
//int a;
//a=(int)x;
//Console.WriteLine(x);
//Console.WriteLine(a);

//double a1 = 3_000_000_000;
//int x1;
//x1=(int)a1;
//Console.WriteLine(x1);
//Console.WriteLine(a1);

//convert string to integer
//string num = "100";
//int number=Convert.ToInt32(num);
//Console.WriteLine(number);

//string num = "5";
//int number = int.Parse(num);

//convert integer to string
//int num1 = 100;
//string num2=num1.ToString();
//Console.WriteLine(num2);

//string input = "1b";
//int numb=int.Parse(input);
//Console.WriteLine(numb);
//Console.ReadLine();

//string input = "1b"; int num; 
//bool result = int.TryParse(input, out num);
//Console.WriteLine(result);

//int a = 5;
//int b = a++;
//Console.WriteLine(b);
//Console.WriteLine(a);

//int a1 = 5;
//int b1 = ++a1;
//Console.WriteLine(b1);
//Console.WriteLine(a1);

//and 111
//Console.WriteLine(false&&true&&true&&true);
//or 000
//Console.WriteLine(false||true||true||true);

//bool isa= true;
//Console.WriteLine(!isa);
//Console.WriteLine(!!!!!!!!!!isa);

//null is special,nothing

//nullable->able to be null
//int? demoNullable = null;
//if (demoNullable.HasValue)
//{
//    int value=demoNullable.Value;
//    Console.WriteLine(value);
//}
//else
//{
//    Console.WriteLine("null");
//}

//null coalesing
//int? x = null;
//int y = x ?? 10;
//Console.WriteLine(y);

//int? a = 20;
//int b = a ?? 30;
//Console.WriteLine(b);

//int? x = 10;
//int? y = null;
//x ??= y;//x=x??y
//Console.WriteLine(x);

//is||is not
//var input = "check";
//var isString= input is string;
//Console.WriteLine(isString);

//int? a = 0;
//var isNotNull = a is not null;
//Console.WriteLine(isNotNull);

//
//float a,b,area;
//Console.WriteLine("Hello");
//Console.WriteLine("Enter length: ");
//a = float.Parse(Console.ReadLine()) ;
//Console.WriteLine("Enter width:");
//b = float.Parse(Console.ReadLine());
//Console.WriteLine("Area={0}",a*b);

//float r;
//Console.WriteLine("Enter r: ");
//r = float.Parse(Console.ReadLine()) ;
//Console.WriteLine("Area={0}", Math.Pow(r,2)*Math.PI);

//Exercise
int x, y;
bool kq1, kq2;
Console.WriteLine("find min and max");
do
{
    Console.Write("Enter a: ");
    string a = Console.ReadLine();
    kq1 = int.TryParse(a, out x);
} while (!kq1);

do
{
    Console.Write("Enter b: ");
    string b = Console.ReadLine();
    kq2 = int.TryParse(b, out y);
} while (!kq2);
Console.Write("min is " + Math.Min(x, y));
Console.WriteLine("\nmax is " + Math.Max(x, y));

//2
Console.WriteLine("\nDemo pow");
Console.WriteLine("result 2^16 is " + Math.Pow(2, 16));
Console.WriteLine("result 2^32 is " + Math.Pow(2, 32));
Console.WriteLine("result 2^64 is " + Math.Pow(2, 64));

//3
Console.WriteLine("\nDemo abs");
Console.WriteLine(Math.Abs(1000));
Console.WriteLine(Math.Abs(-12345));
Console.WriteLine(Math.Abs(-1234.45));

//4
Console.WriteLine("\nDemo round");
Console.WriteLine(Math.Round(1234.4567));
Console.WriteLine(Math.Round(123.345));

//5
Console.WriteLine("\nDemo show ");
int c1, d1;
string c, d;
bool check1, check2;
do
{
    Console.Write("Enter c: ");
    c = Console.ReadLine();
    check1 = int.TryParse(c, out c1);
} while (!check1);
do
{
    Console.Write("Enter d: ");
    d = Console.ReadLine();
    check2 = int.TryParse(d, out d1);
} while (!check2);
System.Console.WriteLine("c={0}" + "\t\nd={1}", c, d);

//6
Console.WriteLine("\nDemo pow");
bool check;
int n;
do
{
    Console.Write("Enter n: ");
    check = int.TryParse(Console.ReadLine(), out n);
} while (!check);
Console.WriteLine("Power of 2^{0}: " + Math.Pow(2, n), n);

//7
//F=(C*9/5)+ 32
Console.WriteLine("Convert tempertures=>fahrenheit and revert ");
int cel;
bool flag;
do
{
    Console.Write("Enter the amount of celcius: ");
    flag = int.TryParse(Console.ReadLine(), out cel);
} while (!flag);
Console.Write("Kelvin={0}", cel + 273);
Console.WriteLine("\nFahrenheit={0}", cel * 18 / 10 + 32);
//8
Console.WriteLine("\nGoing For Gold");
int OreGather;
bool chek;
do
{
    Console.Write("Enter the pieces of gathered ore: ");
    chek = int.TryParse(Console.ReadLine(), out OreGather);
} while (!chek);
//For the first 10 pieces->10 coin per piece
var coin = 0;
int OreSell = Math.Min(10, OreGather);
coin =coin + 10 * OreSell;
OreGather = OreGather - OreSell;
//For the next 5 pieces->5 coin per piece
OreSell = Math.Min(5,OreGather);
coin= coin + 5 * OreSell;
OreGather = OreGather - OreSell;
//For the next 3 pieces->2 coin per piece
OreSell = Math.Min(3, OreGather);
coin = coin + 2 * OreSell;
OreGather = OreGather - OreSell;
//For all the remaining of ore->1 coin per piece
coin = coin + OreGather;
Console.Write("Coresponding coin for the pieces of gather one: "+coin);
Console.ReadLine();
