﻿
//Create a program that attempts to read a file that does not exist. Catch the FileNotFoundException and print out an error message that says "File not found".

//try
//{
//    //string filePath = "D:\\hoctap\\C#Class\\readfile\\example.txt";
//    string filePath = "D:\\hoctap\\#Class\\readfile\\example.txt";

//    if (File.Exists(filePath))
//    {
//        Console.WriteLine("Exist file!");
//    }
//    if (!File.Exists(filePath))
//    {
//        throw new FileNotFoundException(filePath);
//    }
//    string fileContent = File.ReadAllText(filePath);
//}
//catch (FileNotFoundException ex)
//{
//    Console.WriteLine("File not found" + ex.Message.ToString());
//}

//Create a program that divides two numbers, but if the second number is zero, catch the DivideByZeroException and print out an error message that says "Cannot divide by zero".
//int a = 10;
////int b = 5;
//int b = 0;
//try
//{
//    if (b==0)
//    {
//        throw new DivideByZeroException();
//    }
//    int result = a / b;
//    Console.WriteLine($"a/b= {result}");
//}
//catch (DivideByZeroException ex)
//{
//    Console.WriteLine("Cannot divide by zero\t" + ex.Message.ToString());
//}

//Create a program that tries to convert a string to an integer, but if the string cannot be converted, catch the FormatException and print out an error message that says "Invalid input".
//string input = "abc";
//try
//{
//    int num = int.Parse(input);
//    Console.WriteLine(num);
//}
//catch (FormatException ex)
//{
//    Console.WriteLine("Invalid input\t" + ex.Message.ToString());
//}

//Create a program that reads user input, but if the input is empty, catch the ArgumentNullException and print out an error message that says "Input cannot be empty".
//try
//{
//    Console.Write("Enter: ");
//    string input = Console.ReadLine();
//    if (string.IsNullOrEmpty(input))
//    {
//        throw new ArgumentNullException();
//    }
//    Console.WriteLine("You entered: " + input);
//}
//catch (ArgumentNullException ex)
//{
//    Console.WriteLine("input can not be empty\t" + ex.Message.ToString());
//}
//Create a program that creates an array, but if the array size is negative, catch the ArgumentOutOfRangeException and print out an error message that says "Array size must be positive".

//try
//{
//    Console.Write("Enter size of arrray: ");
//    int k = int.Parse(Console.ReadLine());
//        if (k <= 0)
//    {
//        throw new ArgumentOutOfRangeException(nameof(k));
//    }
//    else
//    {
//        Console.WriteLine("Successfully create array");
//    }
//    int[] arr = new int[k];
//}
//catch (ArgumentOutOfRangeException ex)
//{

//    Console.WriteLine("Positive please\t" + ex.Message.ToString());
//}

//Create a program that reads a text file and converts the contents to upper case, but if the file cannot be read, catch the IOException and print out an error message that says "Cannot read file".
////string filePath = "D:\\hoctap\\C#Class\\readfile\\example.txt";
//string filePath = "D:\\hoctap\\#Class\\readfile\\example.txt";
//try
//{
//    if (File.Exists(filePath))
//    {
//        string text = File.ReadAllText(filePath);
//        string content = text.ToUpper();
//        Console.WriteLine("Exist file");
//    }
//    else
//    {
//        throw new IOException();
//    }
//}
//catch (IOException ex)
//{
//    Console.WriteLine("Cannot readfile!\t" + ex.Message);
//}
//Create a program that performs a calculation, but if the result is too large to fit in an integer, catch the OverflowException and print out an error message that says "Result too large".
//try
//{
//    int a = int.MaxValue;
//    int b = int.MinValue;
//    Console.WriteLine(a * b);
//}
//catch (OverflowException ex)
//{
//    Console.WriteLine("Overflow\t" + ex.Message);
//}
//Note: These exercises are intended to be done using try-catch blocks, where the code inside the try block is expected to throw the specified exception. The expected output is the error message that should be printed out when the exception is caught.

//FileNotFoundException
//DivideByZeroException
//FormatException
//ArgumentNullException
//ArgumentOutOfRangeException
//IOException
//OverflowException