﻿const int maxStack = 4;
var a = new int[maxStack];
int top = -1;

void Push(int number)
{
    if (top == maxStack - 1)
    {
        throw new InvalidOperationException();
    }
    top++;
    a[top] = number;
}

int Pop()
{
    if (top == -1)
    {
        throw new InvalidOperationException();
    }
    int number = a[top--];
    return number;
}

int Peek()
{
    if (top == -1)
    {
        throw new InvalidOperationException();
    }
    return a[top];
}

Push(1);
Push(2);
Push(3);
Push(4);
Pop();

for (int i = top; i >= 0; i--)
{
    Console.Write(a[i] + " ");
}
