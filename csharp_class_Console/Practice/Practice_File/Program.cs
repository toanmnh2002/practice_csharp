﻿

//Directory.CreateDirectory("D:\\hoctap\\C#Class\\PracticeFile");
string filePath = "D:\\hoctap\\C#Class\\PracticeFile\\Student.txt";
//if (!File.Exists(filePath))
//{
//    File.Create(filePath);
//    Console.WriteLine("Created " + filePath);
//}
//else
//{
//    Console.WriteLine("File existed!");
//}
//try
//{
//    File.WriteAllText(filePath, "Alice: 90 85 92 87\r\nBob: 80 85 75 90\r\nCharlie: 95 85 90 92\r\n");
//}
//catch (IOException ex)
//{
//    Console.WriteLine("Error "+ex.Message.ToString());
//}

if (args.Length >= 1) filePath = args[0];



int[][] grades;
int[][] ReadGrade(string name)
{
    string[] line = File.ReadAllLines(name);
    if (line.Length == 0)
    {
        throw new Exception("file is empty");
    }
    int[][] grades = new int[line.Length][];
    for (int i = 0; i < line.Length; i++)
    {
        string[] Name = line[i].Split(':');
        if (Name.Length != 2)
        {
            throw new Exception($"Invalid data on line {i + 1}.");
        }
        string[] data = Name[1].Split();
        int[] row = new int[data.Length];
        for (int j = 0; j < data.Length; j++)
        {
            if (!int.TryParse(Name[j], out row[j]))
            {
                throw new Exception($"Invalid grade on line {i + 1}, column {j + 1}.");
            }
        }
        grades[i] = row;
    }
    return grades;
}

try
{
    grades = ReadGrade(filePath);
}
catch (Exception ex)
{
    Console.WriteLine($"Error: {ex.Message}");
    return;
}