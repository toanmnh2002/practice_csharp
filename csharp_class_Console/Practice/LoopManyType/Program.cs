﻿using System;
using System.Collections.Generic;

class Program
{
    static void Main(string[] args)
    {
        // Define the options for each category
        Dictionary<int, string> gradeLevels = new Dictionary<int, string> {
            {1, "Freshman"}, {2, "Sophomore"}, {3, "Junior"}, {4, "Senior"}, {5, "Graduate"}
        };
        Dictionary<int, string> seasons = new Dictionary<int, string> {
            {1, "Spring"}, {2, "Summer"}, {3, "Fall"}, {4, "Winter"}, {5, "Monsoon"}
        };
        Dictionary<int, string> programmingLanguages = new Dictionary<int, string> {
            {1, "Java"}, {2, "Python"}, {3, "C#"}, {4, "JavaScript"}, {5, "Ruby"}
        };
        Dictionary<int, string> animals = new Dictionary<int, string> {
            {1, "Mammal"}, {2, "Reptile"}, {3, "Bird"}, {4, "Fish"}, {5, "Insect"}
        };
        Dictionary<int, string> vehicles = new Dictionary<int, string> {
            {1, "Car"}, {2, "Truck"}, {3, "Motorcycle"}, {4, "Boat"}, {5, "Bicycle"}
        };
        Dictionary<int, string> foods = new Dictionary<int, string> {
            {1, "Italian"}, {2, "Chinese"}, {3, "Mexican"}, {4, "American"}, {5, "Indian"}
        };
        Dictionary<int, string> musicGenres = new Dictionary<int, string> {
            {1, "Pop"}, {2, "Rock"}, {3, "Hip-hop"}, {4, "Classical"}, {5, "Jazz"}
        };
        Dictionary<int, string> ageGroups = new Dictionary<int, string> {
            {1, "Child"}, {2, "Teenager"}, {3, "Adult"}, {4, "Senior"}, {5, "Elderly"}
        };
        Dictionary<int, string> electronicDevices = new Dictionary<int, string> {
            {1, "Smartphone"}, {2, "Laptop"}, {3, "Tablet"}, {4, "Smartwatch"}, {5, "Desktop computer"}
        };
        Dictionary<int, string> sports = new Dictionary<int, string> {
            {1, "Soccer"}, {2, "Basketball"}, {3, "Football"}, {4, "Tennis"}, {5, "Swimming"}
        };

        // Loop through each category and ask the user to enter a number between 1 and 5
        for (int i = 0; i < 10; i++)
        {
            Dictionary<int, string> category = null;
            switch (i)
            {
                case 0:
                    category = gradeLevels;
                    Console.Write("Enter a number between 1 and 5 for grade level: ");
                    break;
                case 1:
                    category = seasons;
                    Console.Write("Enter a number between 1 and 5 for season: ");
                    break;
                case 2:
                    category = programmingLanguages;
                    Console.Write("Enter a number between 1 and 5 for programming language: ");
                    break;
                case 3:
                    category = animals;
                    Console.Write("Enter a number between 1 and 5 for type of animal: ");
                    break;
                case 4:
                    category = vehicles;
                    Console.Write("Enter a number between 1 and 5 for type of vehicles: ");
                    break;
            }
        }
    }
}