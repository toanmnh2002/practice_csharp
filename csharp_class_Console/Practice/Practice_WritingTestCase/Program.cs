﻿using Microsoft.VisualBasic;
using System;
using System.Numerics;
using System.Text;

public class Program
{
    public static void Main()
    {
        //Console.WriteLine(ReturnString("hello"));
        //var a = new List<int>() { 9, 4, 5, 2 };
        //Console.WriteLine(ReturnLargestIntegerInList(a));
        //Console.WriteLine(RemoveVowels("hello"));
        //var a = new List<string>() { "tester", "testted", "testest", "hello", "heols" };
        //Console.WriteLine(ReturnString(a));
        var a = new List<string>() { "felix", "mount", "havertz", "madueke", "enzo", "adams" };
        GetStringStartFromList(a);
        foreach (var item in a)
        {
            Console.WriteLine(item);
        }
    }
    //exercise ideas for practicing unit testing:
    //Write a unit test for a method that takes in a string and returns the length of the string.
    //Expectation: The unit test should ensure that the method returns the correct length for a given string.
    public static int ReturnString(string str)
    {
        return str.Length;
    }


    //Write a unit test for a method that takes in two integers and returns the sum of the two integers.
    //Expectation: The unit test should ensure that the method returns the correct sum for two given integers.

    public static int SumTwoInteger(int a, int b)
    {
        return a + b;
    }
    //Write a unit test for a method that takes in an array of integers and returns the sum of the integers.
    //Expectation: The unit test should ensure that the method returns the correct sum for a given array of integers.
    public static int SumArray(int[] arr)
    {
        if (arr.Length == 0)
        {
            throw new ArgumentException("Array can not be empty");
        }
        int sum = 0;
        foreach (var item in arr)
        {
            sum += item;
        }
        return sum;
    }
    //Write a unit test for a method that takes in a string and a character, and returns the number of times the character appears in the string.
    //Expectation: The unit test should ensure that the method returns the correct number of occurrences for a given string and character.
    public static int CountChar(string input, char character)
    {
        int count = 0;
        foreach (var item in input)
        {
            if (item == character)
            {
                count++;
            }
        }
        return count;
    }


    //Write a unit test for a method that takes in a list of integers and returns the largest integer in the list.
    //Expectation: The unit test should ensure that the method returns the correct largest integer for a given list of integers.
    public static int ReturnLargestIntegerInList(List<int> numbers)
    {
        int max = int.MinValue;
        if (numbers == null || numbers.Count == 0)
        {
            throw new ArgumentException("The list cannot be null or empty.");
        }
        //numbers.Sort();
        //int number = numbers[numbers.Count - 1];
        foreach (var item in numbers)
        {
            if (item > max)
            {
                max = item;
            }
        }
        return max;
    }


    //Write a unit test for a method that takes in a string and returns a new string with all vowels removed.
    //Expectation: The unit test should ensure that the method returns the correct string with vowels removed for a given string.
    public static string RemoveVowels(string input)
    {
        string vowels = "ueoaiUEOAI";
        StringBuilder result = new StringBuilder();
        foreach (var item in input)
        {
            if (!vowels.Contains(item))
            {
                result.Append(item);
            }
        }
        return result.ToString();
    }
    //Write a unit test for a method that takes in a list of strings and returns the number of strings that contain the word "test".
    //Expectation: The unit test should ensure that the method returns the correct number of strings that contain "test" for a given list of strings.
    public static int ReturnString(List<string> strings)
    {
        if (strings == null || strings.Count == 0)
        {
            throw new ArgumentException("The list cannot be null or empty.");
        }
        int count = 0;
        foreach (var item in strings)
        {
            if (item.Contains("test"))
            {
                count++;
            }
        }
        return count;
    }
    //Write a unit test for a method that takes in a list of integers and returns a new list with only the even integers.
    //Expectation: The unit test should ensure that the method returns a new list with only even integers for a given list of integers.
    public static List<int> GetEvenNumberFromList(List<int> numbers)
    {
        if (numbers == null || numbers.Count == 0)
        {
            throw new ArgumentException("The list cannot be null or empty.");
        }
        var a = new List<int>();
        foreach (var item in numbers)
        {
            if (item % 2 != 0)
            {
                a.Add(item);
            }
        }
        return a;
    }
    //Write a unit test for a method that takes in a list of strings and returns a new list with only the strings that start with a vowel.
    //Expectation: The unit test should ensure that the method returns a new list with only strings starting with a vowel for a given list of strings.
    public static List<string> GetStringStartFromList(List<string> strings)
    {
        if (strings == null || strings.Count == 0)
        {
            throw new ArgumentException("The list cannot be null or empty.");
        }
        string vowels = "euoaiEUOAI";
        var a = new List<string>();
        foreach (var item in strings)
        {
            if (vowels.IndexOf(item[0]) != -1)
            {
                a.Add(item);
            }
        }
        return a;
    }
    //Write a unit test for a method that takes in a string and returns a new string with all characters in reverse order.
    //Expectation: The unit test should ensure that the method returns a new string with characters in reverse order for a given string.
    public static string ReverseString(string input)
    {
        char[] charArray = input.ToCharArray();
        Array.Reverse(charArray);
        return new string(charArray);
    }

}
